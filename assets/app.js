/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// Use bootstrap 
//import 'bootstrap';
// loads the jquery package from node_modules
import jquery from 'jquery';

// Importation de bootstrap des 'node_modules'
// require('bootstrap');
import bootstrap from 'bootstrap';

// import { createPopper } from '@popperjs/core';

// Importation d'animateCSS une bibliothèque d’animations
import 'animate.css';

// SASS compilation avec webpack
import './sass/app.scss';

// Font Awesome
import '@fortawesome/fontawesome-free/css/all.min.css';
import '@fortawesome/fontawesome-free/js/all.js';


# PROJET D'EXAMEN



## SMTP google mot de passe d'application

1. Allez dans les paramètres de votre compte Google
2. Puis allez dans les paramètres de « Sécurité »
3. Ici, dans le cadre « Connexion à Google » vérifiez que la  « validation en deux étapes » soit bien activée (si ce n’est pas le cas, activée la en suivant les étapes)
4. Cliquez ensuite sur « Mots de passe des applications »
5. Reconnectez-vous
6. Sélectionnez une application à l’aide du menu déroulant, sinon personnalisez la case avec « Caméra » par exemple
7. Cliquez sur « générer »
8. Une fenêtre s’ouvre et le mot de passe généré est indiqué dans le rectangle jaune. **IMPORTANT** : Notez bien ce mot de passe car vous n’y aurez plus accès après.
9. Cliquez sur « OK »
10. Votre mot de passe d’application est créé.
11. 

## Commandes linux

### *chmod* : option -R (récursif)

Modifier les droits d'un fichier d'un dossier.

##### Correspondances des droits en binaire/octale et leurs significations

| Position Binaire | Valeur octale | Droits | Signification           |
| ---------------- | ------------- | ------ | ----------------------- |
| 000              | 0             | - - -  | Aucun droit             |
| 001              | 1             | - -x   | Exécutable              |
| 010              | 2             | - w -  | Ecriture                |
| 011              | 3             | - w x  | Ecrire et exécuter      |
| 100              | 4             | r - -  | Lire                    |
| 101              | 5             | r - x  | Lire et exécuter        |
| 110              | 6             | r w -  | Lire et écrire          |
| 111              | 7             | r w x  | Lire écrire et exécuter |



| Signification | Valeur octale |
| ------------- | ------------- |
| Execution     | 1             |
| Write         | 2             |
| Read          | 4             |

**A retenir** : Pour de finir les droits on additionne les valeurs octale pour chaque type d'utilisateurs

 Propriétaire, Groupe, Les autres.

Exemple :

````
sudo chmod 751 monscript
````

**Propriétaire : 7**  =  1 (Execution ) + 2 (write) +  4 (read)

**Groupe : 3**           =  1 (Execution ) + 2 (write) 

**Les autres :  5**     = 1 (Execution ) +  4 (read)

| Type d'utilisateurs | Propriétaire | Groupe | Les autres |
| ------------------- | ------------ | ------ | ---------- |
| Droits              | r w x        | r - x  | - - x      |
| Position Binaire    | 111          | 101    | 001        |
| Valeur Octale       | 7            | 5      | 1          |



### *chown*

Chown permet de modifier le propriétaire d'un fichier.

Modifier les permissions sur ce dossier (pour que l'installateur de  Joomla! puisse y avoir accès) pour le groupe d'utilisateurs www-data.

````
sudo chown -R eric:www-data dev-royaumedeschampignons/public/upload
````

Puis rendre le répertoire upload (images) accessible en lecture écriture exécution pour le groupe www-data

````
sudo chmod -R 777 dev-royaumedeschampignons/public/upload
````



## Notion de base 

### Autoloading

 C'est le chargement automatique des classes. Sorte d'annuaires qui répertorie le lieu ou sont disponible les classes.

Cela permet d'expliquer à PHP où se trouvent vos classes afin qu'il fasse les require tout seul.

Cette organisation n’est pas spécifique à Symfony, mais à Composer. Composer est capable d’autocharger toutes vos classes, grâce à la configuration de la section **autoload** du fichier **composer.json**.

ex : sans autoloading

On utilise `require` ou `require_once` suivi du chemin d'accès au fichier contenant la classe a importer avec `use`.

````
app
	Controller
		Guestbook
			Myclass.php
		OtherClass
			GetMethodeMyClass.php
````

````
// GetMethodeMyClass.php

use app\Controller\guestbook

require_once 'app/Controller/Guestbook/Myclass.php'
````



ex : avec autoloading

````
// GetMethodeMyClass.php

use app\Controller\guestbook
````

**Le standard PSR-4**

Par défaut, dans un projet Symfony, la section **autoload** contient la configuration suivante :

```
{ 
    "autoload": { 
        "psr-4": { "": "src/" } 
    } 
}
```

Cette dernière configure un autochargement de type PSR-4. Les normes PSR sont des standards définis par les responsables des différents frameworks PHP. 



### L'injection de dépendances

Permet d'instancier automatiquement une classe. (sans utiliser le mot cle new).

Ex: gérer des personnes qui ont chacune une adresse postale

```php
class Address
{
    private $number;
    private $street;
    private $zipcode;
    private $city;

    public function __construct($number, $street, $zipcode, $city)
    {
        $this->number = $number;
        $this->street = $street;
        $this->zipcode = $zipcode;
        $this->city = $city;
    }
}

class Person
{
    private $address;
}
```

Sans injection de dépendance

```php
class Person
{
    private $address;

    public function __construct($number, $street, $zipcode, $city)
    {
        $this->address = new Address($number, $street, $zipcode, $city);
    }
}

$person = new Person(5, 'Allée des Rosiers', 78670, 'Villennes-sur-Seine');
```

Le problème est qu'en procédant ainsi, les classes Person et Address deviennent étroitement couplées : la classe Person est inutilisable sans la classe Address, et pire, le code interne de la première serait impacté par un changement sur la seconde.

Si la classe adress est modifie cad qu'il faut ajouté un nouvel argument alors il faudra modidier le constructeur de la classe person
Donc evité

```php
class Address
{
    // ...
    private $country;

    public function __construct($number, $street, $zipcode, $city, $country)
    {
        // ...
        $this->country = $country;
    }
}
```

Le problème est contourné avec  l'injection de dépendance

```php
class Person
{
    private $address;

    public function __construct(Address $address)
    {
        $this->address = $address;
    }
}
```



Si une classe a besoin d'une instance d'une autre classe, que ce soit dans son constructeur ou dans une autre méthode (un *setter* par exemple), alors elle prend cette instance directement en paramètre et ne s'occupe certainement pas de l'instancier elle-même. Procéder ainsi permet d'écrire du code **découplé**, évitant toute interdépendance entre ses différents composants, ce qui les rend **réutilisables** 



## Pre-requis avec Symfony

- Symfony CLI  les commande dans la console ou terminal
- PHP
- Composer pour la gestion des dépendances coté back
- npm  pour la gestion des dépendances coté front



## Mise à jour de WampServer

J'ai choisi  d'utiliser WampServer qui me permet d'installer : 

- un server apache, 
- PHP,
- phpMyAdmin,
- MySQL.

Je mets à jour WampServer partir du dépôt ( https://wampserver.aviatechno.net/ ) :

- wampmanager
- apache v2.4.51
- php v2.4.25
- phpmyadmin 5.11



## Mettre à jour composer

````
$ composer self-update
Upgrading to version 2.2.3 (stable channel).
   
Use composer self-update --rollback to return to version 2.0.7
# spécifer une version
composer self-update 2.0.7
````



## Création du dépôt git



## Initialisation du projet Symfony

````
symfony new ProjetExamen-CDA --full
````



## Lancer le serveur symfony

````
symfony serve:start
````



## Contrôler existence des bundles

Retrouver tous les bundle utilisé par l'application dans  `config/bundles.php`



## Création de la base de données

### Configuration de la base de donnée

 Utiliser a constante DATABASE_URL dans le fichier .env

````php
DATABASE_URL="mysql://identifiant:password@127.0.0.1:3306/mycodb?serverVersion=5.7"
````

### Créer la base de données

````bash
php bin/console doctrine:database:create
````



## Sécurisation de l'application

Certains outils de sécurité liés à HTTP, tels que les cookies de session sécurisés et la protection CSRF, sont fournis par défaut.

Le SecurityBundle fournit toutes les fonctionnalités d’authentification et d’autorisation nécessaires pour sécuriser votre application.

````bash
composer require symfony/security-bundle
````

### Configuration de la sécurité

`config/packages/security.yaml`

````php
security:
    enable_authenticator_manager: true
    # https://symfony.com/doc/current/security.html#registering-the-user-hashing-passwords
    password_hashers:
        Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface: 'auto'
    # https://symfony.com/doc/current/security.html#loading-the-user-the-user-provider
    providers:
        users_in_memory: { memory: null }
    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        main:
            lazy: true
            provider: users_in_memory

            # activate different ways to authenticate
            # https://symfony.com/doc/current/security.html#the-firewall

            # https://symfony.com/doc/current/security/impersonating_user.html
            # switch_user: true

    # Easy way to control access for large sections of your site
    # Note: Only the *first* access control that matches will be used
    access_control:
        # - { path: ^/admin, roles: ROLE_ADMIN }
        # - { path: ^/profile, roles: ROLE_USER }
````

1. **L’Utilisateur (`providers`)**
   Toute section sécurisée de votre application a besoin d’un concept d’utilisateur. Le fournisseur d’utilisateurs charge les utilisateurs à partir de  n’importe quel stockage (par exemple, la base de données) sur la base  d’un « identifiant d’utilisateur » (par exemple, l’adresse e-mail de  l’utilisateur);

2. **Le pare-feu et l’authentification des utilisateurs (`firewalls`)**
   Le pare-feu est au cœur de la sécurisation de votre application. Le pare feu vérifie si la requête utilisateur a besoin d'une authentification . Le pare-feu se charge également d’authentifier cet utilisateur (par exemple, à l’aide d’un formulaire de connexion); 

3. **Contrôle d’accès (autorisation) (`access_control`)**
   À l’aide du contrôle d’accès et du vérificateur d’autorisation, vous  contrôlez les autorisations requises pour effectuer une action  spécifique ou visiter une URL spécifique. 

### L’Utilisateur (`providers`)

#### Créer la classe d’utilisateurs

**Utiliser la commande `make:user`** du MakerBundle pour créer une entites Doctrine qui implémente `UserInterface`.

````php
$ bin/console make:user

The name of the security user class (e.g. User) [User]:     
 >User

Do you want to store user data in the database (via Doctrine)? (yes/no) [yes]:
 >yes

Enter a property name that will be the unique "display" name for the user (e.g. email, username, uuid) [email]:
 > email
   
Will this app need to hash/check user passwords? Choose No if passwords are not needed or will 
be checked/hashed by some other system (e.g. a single sign-on server).

Does this app need to hash/check user passwords? (yes/no) [yes]:
 >yes

created: src/Entity/User.php
created: src/Repository/UserRepository.php
updated: src/Entity/User.php
updated: config/packages/security.yaml


Success!


Next Steps:
- Review your new App\Entity\User class.
- Use make:entity to add more fields to your User entity and then run make:migration.        
 - Create a way to authenticate! See https://symfony.com/doc/current/security.html
````

 La commande `make:user` permet le chargement de l'utilisateur.

La configuration utilise Doctrine pour charger `User` en utilisant la propriété `email` comme « identificateur utilisateur ».

```php
# config/packages/security.yaml
security:
    # ...

    providers:
        app_user_provider:
            entity:
                class: App\Entity\User
                property: email
```

#### Créer les tables en exécutant une migration

````bash
php bin/console make:migration
php bin/console doctrine:migrations:migrate
````

#### Enregistrement de l'utilisateur : Hashage du mot de passe

 La classe User doit implémenter `PasswordAuthenticatedUserInterface` pour **l'encryptage du mot de passe.**

````php
// src/Entity/User.php

// ...
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    // ...

    /**
     * @return string the hashed password for this user
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
````

#### Configuration de l'encryptage, par défaut Symfony utilise  `bcrypt`.

````yaml
# config/packages/security.yaml
security:
    # ...
    password_hashers:
        # Use native password hasher, which auto-selects and migrates the best
        # possible hashing algorithm (starting from Symfony 5.3 this is "bcrypt")
        Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface: 'auto'
````

#### hashage du mot de passe

Utiliser le service `UserPasswordHasherInterface` pour le faire avant d’enregistrer vos utilisateurs dans la base de données

````php
// src/Controller/RegistrationController.php
namespace App\Controller;

// ...
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class RegistrationController extends AbstractController
{
    public function index(UserPasswordHasherInterface $passwordHasher)
    {
        // ... e.g. get the user data from a registration form
        $user = new User(...);
        $plaintextPassword = ...;

        // hash the password (based on the security.yaml config for the $user class)
        $hashedPassword = $passwordHasher->hashPassword(
            $user,
            $plaintextPassword
        );
        $user->setPassword($hashedPassword);

        // ...
    }
}
````

#### Configurer le contrôleur d’enregistrement et ajouter la vérification de l’adresse e-mail.

````bash
composer require symfonycasts/verify-email-bundle
````

Retour console

````bash
$ composer require symfonycasts/verify-email-bundle
Using version ^1.5 for symfonycasts/verify-email-bundle
./composer.json has been updated
Running composer update symfonycasts/verify-email-bundle
Loading composer repositories with package information
Updating dependencies
Lock file operations: 1 install, 0 updates, 0 removals
  - Locking symfonycasts/verify-email-bundle (v1.5.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 1 install, 0 updates, 0 removals
  - Installing symfonycasts/verify-email-bundle (v1.5.0): Extracting archive
Generating optimized autoload files
composer/package-versions-deprecated: Generating version class...
composer/package-versions-deprecated: ...done generating version class
110 packages you are using are looking for funding.
Use the `composer fund` command to find out more!

Symfony operations: 1 recipe (4d408bf188987d7040ab8f418604afcb)
  - Configuring symfonycasts/verify-email-bundle (>=v1.5.0): From auto-generated recipe
Executing script cache:clear [OK]
Executing script assets:install public [OK]
````

#### Création du formulaire

````bash
$ php bin/console make:registration-form

 Creating a registration form for App\Entity\User

 Do you want to add a @UniqueEntity validation annotation on your User class to make sure duplicate accounts aren't created? (yes/no) [yes]:  
 > yes

 Do you want to send an email to verify the user's email address after registration? (yes/no) [yes]:
 > yes

 By default, users are required to be authenticated when they click the verification link that is emailed to them.
 This prevents the user from registering on their laptop, then clicking the link on their phone, without
 having to log in. To allow multi device email verification, we can embed a user id in the verification link.

 Would you like to include the user id in the verification link to allow anonymous email verification? (yes/no) [no]:
 > no

 What email address will be used to send registration confirmations? e.g. mailer@your-domain.com:
 > myEmail@gmail.com

 What "name" should be associated with that email address? e.g. "Acme Mail Bot":
 >



 [ERROR] This value cannot be blank.


 What "name" should be associated with that email address? e.g. "Acme Mail Bot":
 > Mail bot

 Do you want to automatically authenticate the user after registration? (yes/no) [yes]:
 > yes

 ! [NOTE] No Guard authenticators found - so your user won't be automatically authenticated after registering.

 What route should the user be redirected to after registration?:
  [0 ] _wdt
  [1 ] _profiler_home
  [2 ] _profiler_search
  [3 ] _profiler_search_bar
  [4 ] _profiler_phpinfo
  [5 ] _profiler_search_results
  [6 ] _profiler_open_file
  [7 ] _profiler
  [8 ] _profiler_router
  [9 ] _profiler_exception
  [10] _profiler_exception_css
  [11] _preview_error
 > 1
1

 updated: src/Entity/User.php
 updated: src/Entity/User.php
 created: src/Security/EmailVerifier.php
 created: templates/registration/confirmation_email.html.twig
 created: src/Form/RegistrationFormType.php
 created: src/Controller/RegistrationController.php
 created: templates/registration/register.html.twig
           
  Success! 
           
 Next:
 1) In RegistrationController::verifyUserEmail():
    * Customize the last redirectToRoute() after a successful email verification.
    * Make sure you're rendering success flash messages or change the $this->addFlash() line.
 2) Review and customize the form, controller, and templates as needed.
 3) Run "php bin/console make:migration" to generate a migration for the newly added User::isVerified property.

 Then open your browser, go to "/register" and enjoy your new form!
````

**Mettre à jour la base de données**

````bash
php bin/console make:migration
php bin/console doctrine:migrations:migrate
````

#### Configurer l'envoi de mail dans le fichier `.env` à la racine du projet

- `MailHog` : en cours de développement on peut utiliser ce serveur SMTP, il suffit de télécharger l'application, ceci lancera un server SMTP virtuel en local.

  Pour consulter les mails ouvrir le navigateur et récupérer l'adresse dans `MailHog` ici   `localhost:8025`

  ````bash
  2021/10/27 20:30:01 Using in-memory storage
  [HTTP] Binding to address: 0.0.0.0:8025
  2021/10/27 20:30:01 [SMTP] Binding to address: 0.0.0.0:1025
  2021/10/27 20:30:01 Serving under http://0.0.0.0:8025/
  ````

  Puis ajouter la config dans le fichier .env

  ````bash
  // .env
  
  ###> symfony/mailer ###
   MAILER_DSN=${MAILER_DSN}
  ````

 Puis ajouter les informations sensibles au fichier .env.local (il ne sera pas visible dans le dépôt)

  ````bash
  // .env.local
  

  MAILER_DSN=smtp://localhost:1025
  # OU avec à partir d'un server SMTP
  # MAILER_DSN=smtp://myMail@gmail.com:*clé_d_application*@smtp.gmail.com:465
 ````

- Avec un fournisseur SMTP comme **`SendInBlue`**.

- Avec le **SMTP Google**, créer un compte et choisissez validation en 2 étapes puis ajouter un mot de passe d’ applications.



### Le pare-feu

#### L'authentification des utilisateurs

Le pare-feu définit quelles parties de votre application sont sécurisées et *comment* vos utilisateurs pourront s’authentifier (par exemple, formulaire de connexion, jeton API, etc.).

#### Formulaire de connexion

````bash
php bin/console make:controller Login
````

````bash
 created: src/Controller/LoginController.php
 created: templates/login/index.html.twig
````

##### Activez l’authentificateur de connexion au formulaire à l’aide du paramètre `form_login`

````yaml
# config/packages/security.yaml
security:
    # ...

    firewalls:
        main:
            # ...
            form_login:
                # "login" is the name of the route created previously
                login_path: login
                check_path: login
````

##### Une fois activé, le système de sécurité **redirige les visiteurs non authentifiés** vers le `login_path`.

````php
// src/Controller/LoginController.php

<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    /**
     * @Route("/authenticator", name="login")
     */
    public function index(AuthenticationUtils $authenticationUtils): Response
    {
         // get the login error if there is one
         $error = $authenticationUtils->getLastAuthenticationError();

         // last username entered by the user
         $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('login/index.html.twig', [
            'controller_name' => 'LoginController',
            'last_username' => $lastUsername,
            'error'         => $error,
            'title'         =>'login'
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
````

##### Convention 

- `<form>` envoie une requête `POST` à l’itinéraire `login` car c’est ce que vous avez configuré comme `check_path` sous la clé `form_login` dans `security.yaml`
- Le champ de nom d’utilisateur (ou quel que soit l'"identifiant » de votre utilisateur, comme un e-mail) a le nom `_username` et le champ de mot de passe a le nom `_password`.

##### Processus

1. L’utilisateur tente d’accéder à une ressource protégée (par exemple, `/admin`);
2. Le pare-feu lance le processus d’authentification en redirigeant l’utilisateur vers le formulaire de connexion (`/login`);
3. La page `/login` affiche le formulaire de connexion via l’itinéraire et le contrôleur créés dans cet exemple ;
4. L’utilisateur envoie le formulaire de connexion à `/login`;
5. Le système de sécurité (c’est-à-dire `form_login` intercepte la demande, vérifie les informations d’identification  soumises par l’utilisateur, authentifie l’utilisateur si elles sont  correctes et renvoie l’utilisateur au formulaire de connexion s’il ne  l’est pas.

##### Protection CSRF

Activer CSRF sur le formulaire de connexion

````yaml
# config/packages/security.yaml
security:
    # ...

    firewalls:
        secured_area:
            # ...
            form_login:
                login_path: login
                check_path: login
+               enable_csrf: true
````

 Utilisez la fonction `csrf_token()` dans le modèle Twig pour générer un jeton CSRF et le stocker en tant que champ masqué du formulaire. 

````twig
{# templates/login.html.twig #}

{% if error %}
	<div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}
<form action="{{ path('login') }}" method="post">
	<label for="email">Email</label>
	<input type="email" class="form-control" id="email" name="_username" value="{{ last_username }}"/>
	
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" name="_password"/>
    
    {# Génère un jeton Token faille CRSF #}
    <input type="hidden" name="_csrf_token" value="{{ csrf_token('authenticate') }}" >
</form>
<a href="{{path('app_forgot_password_request')}}" class="text-warning">Mot de passe oublié ?</a>
````

Déconnexion

````yaml
# config/packages/security.yaml
security:
    # ...

    firewalls:
        main:
            # ...
            logout:
                path: app_logout
    #...
````

#### Réinitialiser le mot de passe

````bash
composer require symfonycasts/reset-password-bundle
````

Utilisez la commande `make:reset-password`

````bash
php bin/console make:reset-password
````

Rerour console

````bash
$ php bin/console make:reset-password

Let's make a password reset feature!
====================================

 Implementing reset password for App\Entity\User

- ResetPasswordController -
---------------------------

 A named route is used for redirecting after a successful reset. Even a route that does not exist yet can be used here.

 What route should users be redirected to after their password has been successfully reset? [app_home]:
 >app_home

- Email -
---------

 These are used to generate the email code. Don't worry, you can change them in the code later!

 What email address will be used to send reset confirmations? e.g. mailer@your-domain.com:
 > dwwm2021@gmail.com       

 What "name" should be associated with that email address? e.g. "Acme Mail Bot":
 > Mail Bot

 created: src/Controller/ResetPasswordController.php
 created: src/Entity/ResetPasswordRequest.php
 updated: src/Entity/ResetPasswordRequest.php
 created: src/Repository/ResetPasswordRequestRepository.php
 updated: src/Repository/ResetPasswordRequestRepository.php
 updated: config/packages/reset_password.yaml
 created: src/Form/ResetPasswordRequestFormType.php
 created: src/Form/ChangePasswordFormType.php
 created: templates/reset_password/check_email.html.twig
 created: templates/reset_password/email.html.twig
 created: templates/reset_password/request.html.twig
 created: templates/reset_password/reset.html.twig

 
  Success! 
 

 Next:
   1) Run "php bin/console make:migration" to generate a migration for the new "App\Entity\ResetPasswordRequest" 
entity.
   2) Review forms in "src/Form" to customize validation and labels.
   3) Review and customize the templates in `templates/reset_password`.
   4) Make sure your MAILER_DSN env var has the correct settings.
   5) Create a "forgot your password link" to the app_forgot_password_request route on your login form.

 Then open your browser, go to "/reset-password" and enjoy!
````

**Personnaliser le comportement de réinitialisation** en mettant à jour le fichier `reset_password.yaml`

#### Chargement de l'utilisateur

```yaml
# config/packages/security.yaml
security:
    # ...providers:
    app_user_provider:
        entity:
            class: App\Entity\User
            property: email
```



### Permission - Contrôle d’accès (autorisation)

Restreindre les accès a certaines route comme l'espace d'administration.

#### Rôles

ROLE_ADMIN, ROLE_USER, ROLE_EDITOR,ROLE_MODERATOR...

#### Restreindre l'accès à certaines URL

````
# config/packages/security.yaml
security:
    # ...

    access_control:
        # change '/admin' by the URL used by your Dashboard
        - { path: ^/admin, roles: ROLE_ADMIN }
        # ...
````

#### Restreindre l’accès aux actions



#### Rôles hiérarchiques

Définir des règles d’héritage de rôle en créant une hiérarchie.

```
// config/packages/security.php

// ...

access_control:
    - { path: ^/admin, roles: ROLE_ADMIN }
    - { path: ^/profile, roles: ROLE_USER }

role_hierarchy:
    ROLE_EDITOR: ROLE_USER
    ROLE_ADMIN: ROLE_EDITOR
```



#### Sécurisation des modèles d’URL (access_control)

Pour sécuriser une partie de votre application consiste à sécuriser un modèle d’URL entier dans `security.yaml`. Par exemple, pour exiger `ROLE_ADMIN` pour toutes les URL commençant par `/admin`, vous pouvez :

````
# config/packages/security.yaml
security:
    # ...

    firewalls:
        # ...
        main:
            # ...

    access_control:
        # require ROLE_ADMIN for /admin*
        - { path: '^/admin', roles: ROLE_ADMIN }

        # or require ROLE_ADMIN or IS_AUTHENTICATED_FULLY for /admin*
        - { path: '^/admin', roles: [IS_AUTHENTICATED_FULLY, ROLE_ADMIN] }

        # the 'path' value can be any valid regular expression
        # (this one will match URLs like /api/post/7298 and /api/comment/528491)
        - { path: ^/api/(post|comment)/\d+$, roles: ROLE_USER }
````

#### Refuser l’accès depuis l’intérieur d’un contrôleur

````
// GOOD - use of the normal security methods
$hasAccess = $this->isGranted('ROLE_ADMIN');
OU
$this->denyAccessUnlessGranted('ROLE_ADMIN');
````

````
// src/Controller/AdminController.php
// ...

public function adminDashboard(): Response
{
    $this->denyAccessUnlessGranted('ROLE_ADMIN');

    // or add an optional message - seen by developers
    $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');
}
````

#### Refuser l’accès depuis l’intérieur d’un contrôleur avec des annotations

````
// src/Controller/AdminController.php
  // ...
 use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

 /**
  * Require ROLE_ADMIN for *every* controller method in this class.
  *
  * @IsGranted("ROLE_ADMIN")
  */
  class AdminController extends AbstractController
  {
     /**
      * Require ROLE_ADMIN for only this controller method.
      *
      * @IsGranted("ROLE_ADMIN")
      */
      public function adminDashboard(): Response
      {
          // ...
      }
  }
````

#### Récupérer l'utilisateur courant dans un contrôleur

````
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProfileController extends AbstractController
{
    public function index(): Response
    {
        // usually you'll want to make sure the user is authenticated first,
        // see "Authorization" below
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // returns your User object, or null if the user is not authenticated
        // use inline documentation to tell your editor your exact User class
        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        // Call whatever methods you've added to your User class
        // For example, if you added a getFirstName() method, you can use that.
        return new Response('Well hi there '.$user->getFirstName());
    }
}
````

#### Récupération de l’utilisateur à partir d’un service

Si vous devez obtenir l’utilisateur connecté à partir d’un service, utilisez le service [de sécurité](https://api.symfony.com/5.3/Symfony/Component/Security/Core/Security.html) :                         

```
// src/Service/ExampleService.php
// ...

use Symfony\Component\Security\Core\Security;

class ExampleService
{
    private $security;

    public function __construct(Security $security)
    {
        // Avoid calling getUser() in the constructor: auth may not
        // be complete yet. Instead, store the entire Security object.
        $this->security = $security;
    }

    public function someMethod()
    {
        // returns User object or null if not authenticated
        $user = $this->security->getUser();

        // ...
    }
}
```

#### Récupérer l'utilisateur courant dans un modèle

Vérification pour voir si un utilisateur est connecté (IS_AUTHENTICATED_FULLY)

`IS_AUTHENTICATED_FULLY` n’est pas un rôle, mais il agit en quelque sorte comme tel, et chaque utilisateur qui s’est connecté l’aura. 



Dans un modèle Twig, l’objet utilisateur est disponible via la variable `app.user` grâce à la variable d’application globale Twig:


```twig
{% if is_granted('IS_AUTHENTICATED_FULLY') %}
    <p>Email: {{ app.user.email }}</p>
{% endif %}
```

#### Vérifier l’accès dans un modèle

Vérifier si l’utilisateur actuel a un certain rôle, utiliser la fonction `is_granted()` intégrée dans n’importe quel modèle Twig.

```
{% if is_granted('ROLE_ADMIN') %}
    <a href="...">Delete</a>
{% endif %}
```



## Ajout d'attributs dans la classe User

````
bin/console make:entity
````

Entrer le nom de la Entité

Ajouter le nouvel attribut

Symfony ajoute les attributs et génère automatiquement les accesseurs (getters) et mutateurs (setters).

Effectuer une migration.



## Création des autres entités

Créer les autres entités à partir du diagramme de classe.

````
bin/console make:entity
````

Migrations des données

````
bin/console make:migration
bin/console make:doctrine:migrations/migrate
````





## FRONT END - BUNDLE - WebPack Encore



Permet de regrouper des modules JavaScript, transpiler les fichiers SASS (est un langage de script préprocesseur qui est compilé ou interprété en CSS) en CSS, JS et compiler et minifier des ressources.

Cela installera et activera le [WebpackEncoreBundle](https://github.com/symfony/webpack-encore-bundle), créera le répertoire `assets/` ajoutera un fichier `webpack.config.js` et `node_modules/` à `.gitignore`.

````
npm install @symfony/webpack-encore --save-dev
````

Retour console

````
$ composer require symfony/webpack-encore-bundle
Using version ^1.12 for symfony/webpack-encore-bundle
./composer.json has been updated
Running composer update symfony/webpack-encore-bundle
Loading composer repositories with package information
Updating dependencies
Lock file operations: 1 install, 0 updates, 0 removals
  - Locking symfony/webpack-encore-bundle (v1.12.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 1 install, 0 updates, 0 removals
  - Installing symfony/webpack-encore-bundle (v1.12.0): Extracting archive
Generating optimized autoload files
composer/package-versions-deprecated: Generating version class...
composer/package-versions-deprecated: ...done generating version class
111 packages you are using are looking for funding.
Use the `composer fund` command to find out more!

Symfony operations: 1 recipe (17de96faa87f364c04018569c2225ff9)
  - Configuring symfony/webpack-encore-bundle (>=1.9): From github.com/symfony/recipes:master
Executing script cache:clear [OK]
Executing script assets:install public [OK]

 What's next?


Some files have been created and/or updated to configure your new packages.
Please review, edit and commit them: these files are yours.

 symfony/webpack-encore-bundle  instructions:

  * Install Yarn and run yarn install

  * Uncomment the Twig helpers in templates/base.html.twig

  * Start the development server: yarn encore dev-server
````

Installation des dépendances dans le répertoire locale `node_module`

````
npm install
````

Retour console

````
$ npm install
npm WARN deprecated urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
npm WARN deprecated resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated

> core-js@3.19.0 postinstall C:\workspace_symfony\ProjetExamen-CDA\node_modules\core-js
> node -e "try{require('./postinstall')}catch(e){}"

Thank you for using core-js ( https://github.com/zloirock/core-js ) for polyfilling JavaScript standard library!

The project needs your help! Please consider supporting of core-js:
> https://opencollective.com/core-js 
> https://patreon.com/zloirock 
> https://paypal.me/zloirock 
> bitcoin: bc1qlea7544qtsmj2rayg0lthvza9fau63ux0fstcz 

Also, the author of core-js ( https://github.com/zloirock ) is looking for a good job -)

npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@~2.3.2 (node_modules\chokidar\node_modules\fsevents):   
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.3.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})

added 677 packages from 374 contributors and audited 679 packages in 19.523s

72 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
````

### Configuration de webPack

#### Installer sass-loader & sass pour utiliser  `enableSassLoader()`

````
npm install sass-loader@^12.0.0 sass --save-dev
````

**Installation du pre-processeur sass**, ajouter la `webpack.config.js`:

````
    // ./webpack.config.js
    ...
    // enables Sass/SCSS support
    .enableSassLoader()
    ...
````



### Référencement de fichiers image à partir d’un modèle

**Automatiser la copie des ressources** de `assets/images` dans public `/ build /public/build/images`.

#### Installer `files-folder` pour utiliser `copyFiles`

````
 npm install file-loader@^6.0.0 --save-dev
````

````
// webpack.config.js
      // ...
      .setOutputPath('public/build/')

     .copyFiles({
         from: './assets/images',

         // optional target path, relative to the output dir
         to: 'images/[path][name].[ext]',

         // if versioning is enabled, add the file hash too
         //to: 'images/[path][name].[hash:8].[ext]',

         // only copy files matching this pattern
         //pattern: /\.(png|jpg|jpeg)$/
     })
````

**Copier tous les dossiers et leurs fichiers** des `assets/` vers public / build / `public/build/`.

````
    // webpack.config.js
      // ...
      .setOutputPath('public/build/')

    .copyFiles(
        [
         // optional target path, relative to the output dir
        {from: './assets/images', to: 'images/[path][name].[ext]'},
        {from: './assets/fonts', to: 'fonts/[path][name].[ext]'},
        {from: './assets/videos', to: 'videos/[path][name].[ext]'}
    ])
````

#### Lancer WebPack avec le compilateur SASS en mode auto.

````
npm run watch
````

### Ajouter le Framework CSS Bootstrap et popper

````
npm install bootstrap --save-dev
````

Retour console

````
npm WARN bootstrap@5.1.3 requires a peer of @popperjs/core@^2.10.2 but none is installed. You must install peer dependencies yourself.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@2.3.2 (node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.3.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})

+ bootstrap@5.1.3
added 1 package from 2 contributors and audited 684 packages in 3.753s
````

##### Importation Javascript de popper pour Bootstrap

Jquery n'est plus requis depuis la version 5 de Bootstrap.

````
npm install @popperjs/core --save-dev
````

Exemple

````
// ./assets/app.js

// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('bootstrap');

// or you can include specific pieces
// require('bootstrap/js/dist/tooltip');
// require('bootstrap/js/dist/popover');

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});
````

#### Effectuer le rendu dans Twig

 Utilisez la fonction `asset()`

```
{# assets/images/logo.png was copied to public/build/images/logo.png #}
<img src="{{ asset('build/images/logo.png') }}" alt="ACME logo">

{# assets/images/subdir/logo.png was copied to public/build/images/subdir/logo.png #}
<img src="{{ asset('build/images/subdir/logo.png') }}" alt="ACME logo">
```

Activé l’option [json_manifest_path,](https://symfony.com/doc/current/frontend/encore/versioning.html#load-manifest-files) qui indique à la fonction `asset()` de lire les chemins d’accès finaux du fichier `manifest.json`.

### Chargement de ressources à partir de `entrypoints.json` & `manifest.json`

2 fichiers de configuration sont générés : `entrypoints.json` et `manifest.json`.

- `entrypoints.json` est utilisé par les `encore_entry_script_tags()` et `encore_entry_link_tags()` Twig.
- `manifest.json` permet d'obtenir le nom de fichier versionné tels que les fichiers de police ou les fichiers image, il contient également des informations sur les fichiers CSS et  JavaScript.

##### Configuration des styles CSS dans `webpack.config.js`.

````
// ./webpack.config.js

/*
     * ENTRY CONFIG
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('mushrooms', './assets/app.js')
````

La config est stocké dans le fichier app.js

````
// ./assets.app.js

//import 'bootstrap';

require('bootstrap');
````



## FRONT END - Création du conteneur principal

1. - SEO
2. BODY
   - **header** avec barre de navigation ici  `{% include '_navbar.html.twig' %}`
   - **Pages** injecté dans le conteneur Twig ici   `{% block body %}{% endblock %}`
   -  **Footer** `{% include '_footer.html.twig'%}`

````
// template/base.html.twig

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        {# Contrôler la mise en page sur les navigateurs mobiles #}
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        {# HTML Meta Tags #}
        <meta name="author" content="Eric Lanza" />
        <meta name="copyright" content="copyright2020©Lanza" />

        <title>
            {% block title %}{% endblock %}
        </title>

        {# Autoriser l'indexation par les moteurs de recherche #}
        <meta name="robots" content="index">

        {# Facebook Meta Tags #}
        <meta property="og:locale" content="fr_FR" />
        <meta property="og:type" content="website" />
        {% block og %}{% endblock %}
        <meta property="og:image" content="{{ asset('/video/videoOg.gif') }}" />
        <meta property="og:image:width" content="300" />
        <meta property="og:image:height" content="170" />

        {# Twitter Meta Tags #}
        <meta name="twitter:card" content="summary_large_image">
        {% block twitter %}{% endblock %}
        <meta name="twitter:image" content="{{ asset('/video/videoOg.gif') }}">

        {# Chargement du CSS par webPack #}
        {{ encore_entry_link_tags('app') }}
       
        {% block stylesheets %}{% endblock %}
    </head>

    <body>
        <article class="d-flex flex-column baseMinHeight">
            {% block navbar %}
                <header>
                    {% include '_navbar.html.twig' %}
                </header>
            {% endblock %}

            <main class="flex-grow-1">
                {% block body %}{% endblock %}
            </main>
        </article>
        {% block footer %}
        	<footer>
        		{% include '_footer.html.twig'%}
        	</footer>
        {% endblock %}

        {# Gestion des consentements #}
        <section class="cookiesManager__box text-white" id="cookie-root"></section>

         {{ encore_entry_script_tags('app') }} 

        {# {% block javascripts %}{% endblock %} #}
    </body>
</html>
````



### **Barre de navigation**

Création d'un fichier `_navbar.html.twig` dans le dossier Template avec le composant `navbar` de Bootstrap.

#### Modification de la couleur du thème `navbar-dark` de Bootstrap.

Ci dessous je personnalise les valeurs de propriétés existantes de Bootstrap.

````
// assets/themeColorsBS.scss
// Modification de la couleur du theme navbar-dark de Bootstrap.
$navbar-dark-color: white;
$navbar-dark-hover-color: #c2bdb6;
$navbar-dark-active-color: #e08b5e;

// Modification de la couleur du bouton lorsque la navbar s'est effrondrée.
$navbar-dark-toggler-border-color:white;
````

Importer les styles`bootstrap` à partir des dépendances dans les `node_modules` .

```
// ./assets/scss/app.scss

// Personnaliser les variables bootstrap
@import "themeColorsBS";
@import "~bootstrap/scss/bootstrap";
```

#### Activation des liens de navigation avec une extension Twig.

````
<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            // new TwigFilter('is_active', [$this, 'is_active']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('is_active', [$this, 'is_active']),
        ];
    }
	
    public function is_active($nameOfTemplate,$link)
    {
        if($link === $nameOfTemplate){
            return 'active';
        }
    }
}

````

La fonction `is_active` permet d'ajouter la class active de Bootstrap dans le lien de navigation. Le Controller injecte la variable `title`( le nom de la page) dans le Template.

````
// ./templates/_navbar.html.twig
...
<li class="nav-item text-nowrap">
	<a class="nav-link {{ is_active(title,'Guide des champignons') }}" href="{#{{ path('app_listMushrooms') }} #} ">Guide des 		champignons</a>
</li>
...
````



### BODY : injecte les blocks

Dans ce block j'injecte les pages de mon site.

### FOOTER

Bootstrap flexBox pour occuper tout l'écran.



## Création des pages pour chaque liens de navigation.

Home (page d'acceuil)

Mushroom (Liste des champignons sous forme de vignettes)

Forum (l'espace de discussion)

Blog (actualités)

On utilise le maker pour générer coté back les Contrôleurs et coté front les Templates.

````
php bin/console make:controller
````

Retour console

````
 Choose a name for your controller class (e.g. DeliciousPizzaController):
 > Home

 created: src/Controller/HomeController.php
 created: templates/home/index.html.twig   
````



## BUNDLE - KnpPaginator - Intégrer la pagination 

### Installation de KNP Paginator

````
composer require knplabs/knp-paginator-bundle
````

retour console

````
Using version ^5.7 for knplabs/knp-paginator-bundle
./composer.json has been updated
Running composer update knplabs/knp-paginator-bundle
Loading composer repositories with package information
Updating dependencies
Lock file operations: 2 installs, 0 updates, 0 removals
  - Locking knplabs/knp-components (v3.3.0)
  - Locking knplabs/knp-paginator-bundle (v5.7.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 2 installs, 0 updates, 0 removals
  - Downloading knplabs/knp-components (v3.3.0)
  - Installing knplabs/knp-components (v3.3.0): Extracting archive
  - Installing knplabs/knp-paginator-bundle (v5.7.0): Extracting archive
Generating optimized autoload files
composer/package-versions-deprecated: Generating version class...
composer/package-versions-deprecated: ...done generating version class
111 packages you are using are looking for funding.
Use the `composer fund` command to find out more!

Symfony operations: 1 recipe (0ae9315ff532671b6bc004eb78d1700f)
  - Configuring knplabs/knp-paginator-bundle (>=v5.7.0): From auto-generated recipe
````

### Activation de la pagination dans le contrôleur avec un filtre de recherche

j'utilise  la méthode "**paginate**"

````
// src/Controller/MushroomController.php

class MushroomController extends AbstractController
{
    /**
     * @Route(
     *      "/guide-des-champignons/{search}", name="app_mushrooms",
     *      defaults={ "search": null } 
     * )
     */
    public function mushrooms($search, MushroomRepository $mushroomRepository, PaginatorInterface $paginator, Request $request):Response
    {
        // si l'utilisateur effectuer une recherche par nom commun
        $searchMushroomForm = $this->createForm(SearchMushroomFormType::class);
        $searchMushroomForm->handleRequest($request);

        if( $searchMushroomForm->isSubmitted() && $searchMushroomForm->isValid() )
        {
            // getData() contient la valeur soumises dans le formulaire
            $search = $searchMushroomForm->get('name')->getData();
            // reinitialise la pignation
            $request->query->set('page', 1);
            // redirge sur la meme route avec un nouveau paramètre de recherche
            return $this->redirectToRoute('app_mushrooms',['search' => $search ]);
        }

        // Récupère l'ensemble des fiches qui sont autorisé a être affiché, trié dans l'ordre croissant, et inclus un paramètre d recherche .
        $listMushrooms = $mushroomRepository->findByStartcommonName('%'.$search.'%');
        

        $listMushroomsPaginator = $paginator->paginate(
            $listMushrooms, // Requête contenant les données à paginer.
            // $page,
            $request->query->getInt('page', 1), 
            10 // Nombre de résultats par page.
        );

        // PERSONALISER POUR LE RENDU
        $listMushroomsPaginator->setCustomParameters([
            'align' => 'center',
            'size' => 'medium',
            'rounded' => false,
        ]);

        // Rendu de la page 
        return $this->render('mushrooms/mushrooms.html.twig', [
            'controller_name' => 'MushroomsController',
            'title' => 'Guide des champignons',
            'listMushroomsPaginator' => $listMushroomsPaginator,
            'searchMushroomForm' => $searchMushroomForm->createView(),
        ]);
    }
    }
````

### Rendu dans template



#### Template

Une video en fond d'écran

Utiliser des fonction avec une extension twig



Footer



navbar

image du bouton collapse : 



### Installation de la librairie Font Awesome

````
npm install --save @fortawesome/fontawesome-free
````

````
// ./assets/app.js

...
// Font Awesome
import '@fortawesome/fontawesome-free/css/all.min.css';
import '@fortawesome/fontawesome-free/js/all.js';
...
````



### Ajout de fonction Twig

TODO

## Installation de animate CSS

**Animate.css** est une bibliothèque d’animations multi-navigateurs prêtes à l’emploi .

````
npm install animate.css --save
````

````
// ./assets/app.js

...

...
````



## Gestion des consentements des utilisateurs

Développement d'un gestionnaire de consentement en javascript.

````
// ./templates/base.html.twig

<head>
{# Chargement du CSS par webPack #}
	{{ encore_entry_link_tags('cookieManager') }}
</head>
<body>
...
    {# Gestion des consentements #}
    <section class="cookiesManager__box text-white" id="cookie-root"></section>

    {# Webpack import javascript #}
    {{ encore_entry_script_tags('app') }} 
    {{ encore_entry_script_tags('cookieManager') }} 

    {# {% block javascripts %}{% endblock %} #}
</body>
````

Injection avec webpack

````
// ./assets/cookiesManager.js

// Importe le gestionnaire de consentement
import './cookieManager/cookieManager.js';
import './cookieManager/cookieManager.css';
````

Lancer la compilation avec webPack

````
npm run dev
````



## Création des entités et des relations pour la génération de la base de donnée



Création des relations entre les utilisateurs et les fiches descriptives.

````
````

Déclare pour le user une relation `OneToMany` c'est à dire qu'un utilisateur possède plusieurs fiches descriptive.

user : oneToMany

````
 bin/console make:entity
 
 Class name of the entity to create or update (e.g. OrangeGnome):
 > User
User

 Your entity already exists! So let's add some new fields!

 New property name (press <return> to stop adding fields):
 > mushroom

 Field type (enter ? to see all types) [string]:
 > OneToMany
OneToMany
Many

 What class should this entity be related to?:
 > Mushroom
Mushroom
om
 A new property will also be added to the Mushroom class so that you can access and set the related User object from it.

 New field name inside Mushroom [user]:
 > user

 Is the Mushroom.user property allowed to be null (nullable)? (yes/no) [yes]:
 >

 updated: src/Entity/User.php
 updated: src/Entity/Mushroom.php
````



## Générer automatiquement la date de création et la date de mise à jour

### Ecouteur d’évènements avant persistance des données

Doctrine déclenche des événements avant/après l’exécution des opérations d’entité les plus courantes (par `prePersist/postPersist` `preUpdate/postUpdate`

Informer Doctrine que notre entité contient des callbacks de cycle de vie. Grâce à l'annotation `HasLifecycleCallbacks` avant la définition de classe.

````
/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="Il y a déjà un compte avec cet email")
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
	// ...
	
     /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAt(): self
    {
        $this->updated_at = new \DateTimeImmutable();

        return $this;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): self
    {
        $this->created_at = new \DateTimeImmutable();

        return $this;
    }
}
````



## Générer les slugs

Installer le bundle `StofDoctrineExtensionsBundle`, cette extension développé par la communauté permet en autre de générer automatiquement une valeur unique de propriété pour le slug avant que les données soit persisté en BD.

````
composer require stof/doctrine-extensions-bundle
````

Retour console

````
Using version ^1.6 for stof/doctrine-extensions-bundle
./composer.json has been updated
Running composer update stof/doctrine-extensions-bundle
Loading composer repositories with package information
Updating dependencies
Lock file operations: 3 installs, 0 updates, 0 removals
  - Locking behat/transliterator (v1.3.0)
  - Locking gedmo/doctrine-extensions (v3.3.0)
  - Locking stof/doctrine-extensions-bundle (v1.6.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 3 installs, 0 updates, 0 removals
  - Downloading gedmo/doctrine-extensions (v3.3.0)
  - Installing behat/transliterator (v1.3.0): Extracting archive
  - Installing gedmo/doctrine-extensions (v3.3.0): Extracting archive
  - Installing stof/doctrine-extensions-bundle (v1.6.0): Extracting archive
Generating optimized autoload files
composer/package-versions-deprecated: Generating version class...
composer/package-versions-deprecated: ...done generating version class
111 packages you are using are looking for funding.
Use the `composer fund` command to find out more!

Symfony operations: 1 recipe (7f226176d8bfe0e32cc01a6fd6834bcc)
  -  WARNING  stof/doctrine-extensions-bundle (>=1.2): From github.com/symfony/recipes-contrib:master
    The recipe for this package comes from the "contrib" repository, which is open to community contributions.
    Review the recipe at https://github.com/symfony/recipes-contrib/tree/master/stof/doctrine-extensions-bundle/1.2      

    Do you want to execute this recipe?
    [y] Yes
    [n] No
    [a] Yes for all packages, only for the current installation session
    [p] Yes permanently, never ask again for this project
    (defaults to n): y
  - Configuring stof/doctrine-extensions-bundle (>=1.2): From github.com/symfony/recipes-contrib:master
Executing script cache:clear [OK]
Executing script assets:install public [OK]
````

**La liste des bundles se trouve dans `config/bundles.php`**

configuration du fichier `config/packages/stof_doctrine_extensions.yaml`

````
# Read the documentation: https://symfony.com/doc/current/bundles/StofDoctrineExtensionsBundle/index.html
# See the official DoctrineExtensions documentation for more details: https://github.com/Atlantic18/DoctrineExtensions/tree/master/doc/
stof_doctrine_extensions:
    default_locale: fr_FR
    orm:
        default:
            sluggable: true
````

Installer

````
composer require gedmo/doctrine-extensions
````

Ajouter la variable slug dans l'entité soit avec le maker `bin/console make:entity` soit directement dans l'entité concernée puis ajouter le getters ici nous n'avons pas besoin du setter puisque qu'il sera automatiquement généré.



````
bin/console make:entity
````

Importer l'alias dans l’entité concernée avec l'opérateur use

````
<?php

namespace App\Entity;

use App\Repository\MushroomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity(repositoryClass=MushroomRepository::class)
 */
class Mushroom
{
     // ...
     /**
     * @ORM\Column(type="string", length=100)
     */
    private $commonname;
    
    // ...

    /**
    * @Gedmo\Slug(fields={"commonname"})
    * @ORM\Column(type="string", length=150, unique=true)
    */
    private $slug;
    
     // ...
````

Mettre à jour la base de données



## Templates Twig

### Lien interne

````
{#  path('nom_de_ la_route,{query params}')     #}

<a href="{{ path('app_mushroom',{'slug': mushroom.slug}) }}">
````



## BACK END - BUNDLE - VichUploaderBundle 

### Installation

````
composer require vich/uploader-bundle
````

Retour console

````
Using version ^1.18 for vich/uploader-bundle
./composer.json has been updated
Running composer update vich/uploader-bundle
Loading composer repositories with package information
Updating dependencies
Lock file operations: 2 installs, 0 updates, 0 removals
  - Locking jms/metadata (2.5.2)
  - Locking vich/uploader-bundle (1.18.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 2 installs, 0 updates, 0 removals
  - Downloading jms/metadata (2.5.2)
  - Installing jms/metadata (2.5.2): Extracting archive
  - Installing vich/uploader-bundle (1.18.0): Extracting archive
Generating optimized autoload files
composer/package-versions-deprecated: Generating version class...
composer/package-versions-deprecated: ...done generating version class
111 packages you are using are looking for funding.
Use the `composer fund` command to find out more!  

Symfony operations: 1 recipe (04b614f89eff04d1784f455d31831239)
  -  WARNING  vich/uploader-bundle (>=1.7): From github.com/symfony/recipes-contrib:master
    The recipe for this package comes from the "contrib" repository, which is open to community contributions.
    Review the recipe at https://github.com/symfony/recipes-contrib/tree/master/vich/uploader-bundle/1.7      

    Do you want to execute this recipe?
    [y] Yes
    [n] No
    [a] Yes for all packages, only for the current installation session
    [p] Yes permanently, never ask again for this project
    (defaults to n): y
  - Configuring vich/uploader-bundle (>=1.7): From github.com/symfony/recipes-contrib:master
Executing script cache:clear [OK]
Executing script assets:install public [OK]
````

### Configuration du téléchargement de fichiers

1. Donnner un nom au mappeur dans cet exemple `mushroom_images` puis indiquer au bundle où les fichiers a télécharger doivent être stockés (`upload_destination`) et l'URL vers ce repertoire (`uri_prefix`) .

2. Renommer le fichier téléchargé avec l'option `namer`

   - `Vich\UploaderBundle\Naming\UniqidNamer` :  *renommera vos fichiers téléchargés en utilisant un identifiant unique pour le nom et conservera l'extension*


   - `Vich\UploaderBundle\Naming\OrignameNamer` : *renommera vos fichiers téléchargés en utilisant un identifiant unique comme préfixe du nom de fichier et en conservant le nom et l'extension d'origine.*


   - `Vich\UploaderBundle\Naming\PropertyNamer` : *utilisera une propriété ou une méthode pour nommer le fichier.* 


   - `Vich\UploaderBundle\Naming\HashNamer` : *utilisera un hachage de chaîne aléatoire pour nommer le fichier.* 


   - `Vich\UploaderBundle\Naming\Base64Namer` : *générera une chaîne aléatoire décodable en base64 sécurisée pour les URL pour nommer le fichier.*


   - `Vich\UploaderBundle\Naming\SmartUniqueNamer` : *renommera vos fichiers téléchargés en ajoutant un identifiant unique fort au nom d'origine, tout en appliquant une translittération.*


   - `Vich\UploaderBundle\Naming\SlugNamer` : *ne translittérera que le fichier téléchargé. Ensuite, il recherchera si un tel nom existe déjà et, si c'est le cas, ajoutera un numéro progressif (pour assurer l'unicité).*


3. Configurer les évènements du cycle de vie c'est à dire le comportement à adopter lorsque l'entité sont chargées à partir de la base de données, mises à jour ou supprimées. Par exemple: les fichiers doivent-ils être mis à jour ou supprimés en conséquence?

   - `delete_on_remove`: *par défaut `true`, si le fichier doit être supprimé lorsque l' entité est supprimée ;*

   - `delete_on_update`: *par défaut `true`, si le fichier doit être supprimé lorsqu'un nouveau fichier est téléchargé ;*

   - `inject_on_load`: *default `false`, si le fichier doit être injecté dans l'entité téléchargeable lorsqu'il est chargé à partir du magasin de données. L'objet sera une instance de `Symfony\Component\HttpFoundation\File\File`.*

````
# config/services.yaml
parameters:
    app.path.mushroom_images: '/upload/mushrooms'
    # ...

# config/packages/vich_uploader.yaml
vich_uploader:
    # Etape 1
   mushroom_images:
            uri_prefix: '%app.path.mushroom_images%'
            upload_destination: '%kernel.project_dir%/public%app.path.mushroom_images%'
            # Permet de spécifier quel système de "nommage" nous souhaitons, 
            # ici, nommage unique, deux fichiers ne pourront pas avoir le même nom.
            
            # Etape 2
            namer: Vich\UploaderBundle\Naming\UniqidNamer
            
            # Etape 3
            inject_on_load: false # true si le fichier est injecté dans l'entité téléchargeable 
            # lorsqu'il est chargé à partir du magasin de données. 
            # L'objet sera une instance de Symfony \ Component \ HttpFoundation \ File \ File.
            delete_on_remove: true # suppression du fichier à la suppression de l’entité
            delete_on_update: true # suppression du fichier quand un nouveau fichier est envoyé
````

### Préparation des entités à la persistance des images

Lier le mappeur de téléchargement a l'entité.

1. annotez la classe entity  avec l’annotation `Uploadable` importer la class de mappage utilisable avec les annotations.

````
// scr/Entity/Media.php

namespace App\Entity;

use App\Repository\MediaRepository;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=MediaRepository::class)
 * @Vich\Uploadable
 */
class Media
{
	// ...
}
````

2. créer les deux champs nécessaires au fonctionnement du bundle

   1. un champ (par exemple `imageName`) pour stocker dans la base de données sous forme de chaîne  le nom du fichier

   2. un autre champ (par exemple, `imageFile`). Cela stockera l’objet `UploadedFile` après l’envoyé du formulaire.

      Options pour upload

      - `mapping`: obligatoire, le nom de mappage spécifié dans la configuration du bundle à utiliser ;
      - `fileNameProperty`: obligatoire, la propriété qui contiendra le nom du fichier téléchargé ;
      - `size`: la propriété qui contiendra la taille en octets du fichier téléchargé ;
      - `mimeType`: la propriété qui contiendra le type mime du fichier téléchargé ;
      - `originalName`: propriété qui contiendra le nom d’origine du fichier téléchargé.
      - `dimensions`: la propriété qui contiendra les dimensions du **fichier image** téléchargé

````
	// src/Entity/media.php
	
	// ...
	use Vich\UploaderBundle\Mapping\Annotation as Vich;
	// ...
	
	public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }
    
    // ...
    
	 /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $path;

    /**
     * @Vich\UploadableField(mapping="mushroom_images", fileNameProperty="path")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;
````



### Implémenter Sérialisable sur user

Tous les attributs sont inclus par défaut lors de la sérialisation des objets. Il existe deux options pour ignorer certains de ces attributs.

Solution n°2

L'annotation @Ignore devrait permettre d'ignorer cet attribut 

https://symfony.com/doc/current/components/serializer.html#option-1-using-ignore-annotation

Solution n°2

Ajouter des méthodes de sérialisation sur la classe User,  car le problème est que Symfony essaie sérialiser toute les attributs de la classe user alors que l'attribut  `private $avatarFile;` ne doit pas l'être.

````
// src/Entity/User

use Serializable;

// ...
class User implements UserInterface, PasswordAuthenticatedUserInterface, Serializable
{
// ...
public function serialize()
    {
        return serialize(array( 
            $this->id,
            $this->email,
            $this->roles,
            $this->password,
            $this->isVerified,
            $this->createdAt,
            $this->updatedAt,
            $this->lastname,
            $this->firstname,
            $this->pseudo,
            $this->avatarFilename,
            $this->slug,
            $this->forumSubjects,
            $this->forumCommentarys,

        ));
    }

    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->email,
            $this->roles,
            $this->password,
            $this->isVerified,
            $this->createdAt,
            $this->updatedAt,
            $this->lastname,
            $this->firstname,
            $this->pseudo,
            $this->avatarFilename,
            $this->slug,
            $this->forumSubjects,
            $this->forumCommentarys,
        ) = unserialize($serialized);
    }

````





## BACK END - BUNDLE - LiipImagineBundle

 Le package [LiipImagineBundle](https://github.com/liip/LiipImagineBundle) fournit une *boîte à outils d’abstraction de manipulation d’images*

- [Jeux de filtres](https://symfony.com/doc/current/LiipImagineBundle/basic-usage.html): à l’aide de n’importe quel langage de configuration pris en charge par  Symfony (tel que YML et XML), vous pouvez créer des définitions *de jeux de* filtres qui spécifient des routines de transformation. Ceux-ci incluent un ensemble de *filtres* et *de post-processeurs,*ainsi que d’autres paramètres facultatifs.
- [Filtres](https://symfony.com/doc/current/LiipImagineBundle/filters.html): Un certain nombre de filtres intégrés sont fournis, permettant un tableau de transformations d’image courantes. Les exemples incluent [la vignette,](https://symfony.com/doc/current/LiipImagineBundle/filters/sizing.html#filter-thumbnail) [l’échelle,](https://symfony.com/doc/current/LiipImagineBundle/filters/sizing.html#filter-scale) [le recadrage,](https://symfony.com/doc/current/LiipImagineBundle/filters/sizing.html#filter-crop) [la bande](https://symfony.com/doc/current/LiipImagineBundle/filters/general.html#filter-strip)et [le filigrane,](https://symfony.com/doc/current/LiipImagineBundle/filters/general.html#filter-watermark)et bien d’autres. En outre, [les filtres personnalisés](https://symfony.com/doc/current/LiipImagineBundle/filters.html#filter-custom) sont pris en charge.
- [Post-processeurs](https://symfony.com/doc/current/LiipImagineBundle/post-processors.html): Un certain nombre de post-processeurs intégrés sont fournis, permettant la modification du fichier binaire résultant créé par les filtres. Les exemples incluent [JpegOptim](https://symfony.com/doc/current/LiipImagineBundle/post-processors/jpeg-optim.html#post-processor-jpegoptim), [OptiPNG](https://symfony.com/doc/current/LiipImagineBundle/post-processors/png-opti.html#post-processor-optipng), [MozJpeg](https://symfony.com/doc/current/LiipImagineBundle/post-processors/jpeg-moz.html#post-processor-mozjpeg)et [PngQuant](https://symfony.com/doc/current/LiipImagineBundle/post-processors/png-quant.html#post-processor-pngquant). En outre, [les post-processeurs personnalisés](https://symfony.com/doc/current/LiipImagineBundle/post-processors.html#post-processors-custom) sont pris en charge.

Installation de LiipImagineBundle

````
composer require liip/imagine-bundle
````

Activation du bundle automatique grace à Symfony Flex

Retour console

````
Some files have been created and/or updated to configure your new packages.
Please review, edit and commit them: these files are yours.

 liip/imagine-bundle  instructions:

  Configure your transformations:
    1. You MUST verify and uncomment the configuration in config/packages/liip_imagine.yaml.
    2. You MAY configure your image transformation library (gmagick, imagick, or gd [default]).
    3. You MAY define custom transformation definitions under the filter_sets option.
````

Ajout d'un filtre pour les images 

````
// config/packages/liip_imagine.yaml


// ...

# the name of the "filter set"
	my_thumb:

        # adjust the image quality to 75%
        quality: 75

        # list of transformations to apply (the "filters")
        filters:

            # create a thumbnail: set size to 120x90 and use the "outbound" mode
            # to crop the image when the size ratio of the input differs
            thumbnail: { size: [120, 120], mode: outbound }

            # create a 2px black border: center the thumbnail on a black background
            # 4px larger to create a 2px border around the final image
            background: { size: [124, 94], position: center, color: '#000000' }
````

Ajout du filtre dans le template

````

<img src="{{ vich_uploader_asset(app.user, 'avatarFile') | imagine_filter('my_thumb') }}" alt="Avatar"
	class="userProfil__image--size rounded bg-light border border-secondary mb-3 p-1" />
````







## BACK END - BUNDLE -`EasyAdminBundle` - DashBoard

### Installation du bundle `easyAdminBundle`

````
composer require easycorp/easyadmin-bundle
````

retour console

````bash
Using version ^3.5 for easycorp/easyadmin-bundle
./composer.json has been updated
Running composer update easycorp/easyadmin-bundle
Loading composer repositories with package information
Restricting packages listed in "symfony/symfony" to "5.3.*"
Updating dependencies
Lock file operations: 3 installs, 0 updates, 0 removals
  - Locking easycorp/easyadmin-bundle (v3.5.14)
  - Locking symfony/polyfill-uuid (v1.23.0)
  - Locking symfony/uid (v5.3.10)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 3 installs, 0 updates, 0 removals
  - Downloading symfony/uid (v5.3.10)
  - Downloading easycorp/easyadmin-bundle (v3.5.14)
  - Installing symfony/polyfill-uuid (v1.23.0): Extracting archive
  - Installing symfony/uid (v5.3.10): Extracting archive
  - Installing easycorp/easyadmin-bundle (v3.5.14): Extracting archive
Generating optimized autoload files
composer/package-versions-deprecated: Generating version class...
composer/package-versions-deprecated: ...done generating version class
114 packages you are using are looking for funding.
Use the `composer fund` command to find out more!

Symfony operations: 1 recipe (3561ad644f9b330a193038a0556d79b5)
  - Configuring easycorp/easyadmin-bundle (>=3.0): From github.com/symfony/recipes:master
````



### Installer le bundle dans le dossier asset

````
bin/console assets:install
````

### Configuration du DashBoard

Le tableau de bord est liés aux ressources  (entités)

````bash
php bin/console make:admin:dashboard
````

#### Afficher une page au démarrage du tableau de bord

````php
	/**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);
        return $this->redirect($routeBuilder->setController(MushroomCrudController::class)->generateUrl());

    }
````

#### Traduction 

Pour obtenir une traduction en français des champs et menu contextuels d'esayAdmin je dois modifier le fichier de translation.

````php
// config/packages/translation.php

framework:
    default_locale: fr
    translator:
        default_path: '%kernel.project_dir%/translations'
        fallbacks:
            - fr
````

#### Ajouter un menu de navigation au dashboard

````php
// 

public function configureMenuItems(): iterable
    {
        return[
            MenuItem::linktoDashboard('Acceuil back office','fas fa-user-lock'),

            MenuItem::section('Utilisateurs','fas fa-users'),
                // MenuItem::linkToCrud('Liste des membres', 'fas fa-users','User::class'),
                // ->setPermission('ROLE_ADMIN'),

            MenuItem::section('Guide des champignons'),
            MenuItem::linkToCrud('Fiches signalétique', 'fas fa-list', Mushroom::class),
                // ->setPermission('ROLE_ADMIN'),
            // MenuItem::linkToCrud('Photos', 'fas fa-camera', Media::class),
                // ->setController(ImagesCrudController::class),

            MenuItem::section('Forum','far fa-comments'),
            MenuItem::linkToRoute('Les sujets', 'fa fa-home',''),
            MenuItem::linkToRoute('Les commentaires', 'fa fa-home',''), 
            
            MenuItem::section('Blog','fab fa-blogger'),
            MenuItem::linkToRoute('Les articles', 'fa fa-home',''),
            MenuItem::linkToRoute('Les commentaires', 'fa fa-home',''),

            MenuItem::section('Navigation'),
            MenuItem::linkToRoute('Accueil', 'fa fa-home','app_home'),
            MenuItem::linkToRoute('Guide des champignons', 'fas fa-book-reader','app_mushrooms'),
            MenuItem::linkToRoute('Forum', 'far fa-comments',''),
            MenuItem::linkToRoute('Blog', 'fab fa-blogger',''),
            // Renvoie à une URL via un nom de la route
            // MenuItem::linkToRoute('Retour à l\'Accueil', 'fas fa-undo','app_home'),
            // Renvoie à une URL relative ou absolue (si pas de class alors spécifier que la valeur est null)
            // MenuItem::linkToUrl('Accueil', 'fas fa-undo','home'),
            //Renvoie  l'URL que l'utilisateur doit visiter pour se déconnecter de l'application
            // Definit par la propritété firewall du fichier de config security
            MenuItem::section(),
            MenuItem::linkToLogout('Déconnexion', 'fas fa-sign-out-alt'),
        ];
    }
````



### Ajout de contrôleurs CRUD pour chaque ressources (entities)

````bash
php bin/console make:admin:crud
````

Les quatre pages principales des contrôleurs CRUD sont :

- `index`, affiche une liste d’entités qui peuvent être paginées, triées par  colonne et affinées avec des requêtes de recherche et des filtres;
- `detail`, affiche le contenu d’une entité donnée ;
- `new`, permet de créer de nouvelles instances d’entité ;
- `edit`, permet de mettre à jour n’importe quelle propriété d’une entité donnée.



Arguments de méthode : `index`, `detail`, `edit`  et `new`

 utiliser des constantes : `Crud::PAGE_INDEX`, `Crud::PAGE_DETAIL`, `Crud::PAGE_EDIT` et `Crud::PAGE_NEW`

####  Configuration du CRUD

Modifier le titre, les noms de champs de la page, recherche et pagination.

````php
// scr/Controller/Admin/MushroomCrudController.php

// ...

// Configuration du titre
public function configureCrud(Crud $crud): Crud
{
	return $crud
		->setEntityLabelInPlural('Champignons')
		->setEntityLabelInSingular('Ajouter une fiche')
		->setDateIntervalFormat('%%y Year(s) %%m Month(s) %%d Day(s)')
		->setTimezone('Europe/Paris')
        // nombre de ligne / page
		->setPaginatorPageSize(10)
        // nombre de page dans le controle de pagination "< Previous | Next >" pager
        ->setPaginatorRangeSize(4)
	;
}
````

#### Nommer les champs des entités, et gérer l'affichage en fonction des pages CRUD (`index`, `detail`, `edit`, `new`)

````bash
// scr/Controller/Admin/MushroomCrudController.php

   // ...

use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;

public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addPanel('Dénomination'),
            // ID
            IdField::new('id')->onlyOnIndex(),

            // DATE
            DateTimeField::new('created_at','Créé le')->onlyOnDetail(),
            DateTimeField::new('updated_at','Modifié le')->onlyOnDetail(),

            // DESCRIPTION
            TextField::new('commonname','Nom commun')
                ->setHelp('Ce champ est obligatoire.'),
            TextField::new('latinname','Nom latin')
                ->hideOnIndex(),
            // TextField::new('localname','Nom locale')->hideOnIndex(),

            // PROPRIETAIRE DU DOCUMENT
            FormField::addPanel('Editeur du document')->addCssClass(''),
                AssociationField::new('user','Auteur')->addCssClass('font-weight-normal'),
                // ImageField::new('user','Auteur')->addCssClass('font-weight-normal')->
                //  ->setBasePath('/build/images/upload/users/'),

            // DOCUMENTATION VISIBLE
            FormField::addPanel('Espace public')
                ->addCssClass('')
                ->setHelp('Accessibilité aux visiteurs et membres du site web.'),
                BooleanField::new('visibility','Visible'),

            FormField::addPanel('Caractéristiques')
                ->addCssClass(''),
                TextareaField::new('hat','Chapeau')->addCssClass('font-weight-normal')->hideOnIndex(),
                // AssociationField::new('lamellatype','Type de lamelle')->addCssClass('font-weight-normal')->hideOnIndex(),
                TextareaField::new('lamella','Lamelle')
                    ->addCssClass('')
                    ->hideOnIndex(),
                TextareaField::new('foot','Pied')
                    ->addCssClass('font-weight-normal')
                    ->hideOnIndex(),
                TextareaField::new('flesh','Chair')
                    ->addCssClass('font-weight-normal')
                    ->hideOnIndex(),
                TextareaField::new('habitat','Habitat')
                    ->addCssClass('font-weight-normal')->hideOnIndex(),
                TextareaField::new('comment','Commentaire')->addCssClass('font-weight-normal')
                    ->hideOnIndex(),
            AssociationField::new('edibility','Comestible')
                ->addCssClass('font-weight-normal')
                ->hideOnIndex(),

            // CollectionField::new('images','Images')
            //     ->setEntryType(ImagesType::class)
            //     ->setTranslationParameters(['form.label.delete'=>'Supprimer']),

            // DateTimeField::new('created_at','Créé le')->onlyOnIndex(),
            // DateTimeField::new('updated_at','Modifié le')->onlyOnIndex(),
        ];
    }
````

#### Méthode pour afficher ou masquer des champs en fonction des pages du CRUD

````
    hideOnIndex()
    hideOnDetail()
    hideOnForm() (masque le champ à la fois dans les pages edit et dans les new pages)
    hideWhenCreating()
    hideWhenUpdating()
    onlyOnIndex()
    onlyOnDetail()
    onlyOnForms() (masque le champ dans toutes les pages, à l’exception edit et new)
    onlyWhenCreating()
    onlyWhenUpdating()

````



**ATTENTION**

Pour renvoyer une chaine de caractère dans le champ de l'association ajouter à l'entité la méthode toString

````
// src/entity/User
public function __toString()
    {
        return $this->email;
    }
````



#### Actions intégrées du CRUD

Voici les actions intégrées incluses par défaut dans chaque page 

- Page `Crud::PAGE_INDEX` (`'index'`):
  - Ajouté par défaut : `Action::EDIT`, `Action::DELETE`, `Action::NEW`
  - Autres actions disponibles : `Action::DETAIL`
- Page `Crud::PAGE_DETAIL` (`'detail'`):
  - Ajouté par défaut : `Action::EDIT`, `Action::DELETE`, `Action::INDEX`
  - Autres actions disponibles: -
- Page `Crud::PAGE_EDIT` (`'edit'`):
  - Ajouté par défaut : `Action::SAVE_AND_RETURN`, `Action::SAVE_AND_CONTINUE`
  - Autres actions disponibles : `Action::DELETE`, `Action::DETAIL`, `Action::INDEX`
- Page `Crud::PAGE_NEW` (`'new'`):
  - Ajouté par défaut : `Action::SAVE_AND_RETURN`, `Action::SAVE_AND_ADD_ANOTHER`
  - Autres actions disponibles : `Action::SAVE_AND_CONTINUE`, `Action::INDEX`

````
// Ajouter la vue détail au menu contextuel
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

// ...
public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }
````

#### Restreindre les actions (EDIT, DELETE) pour les pages `index`, `edit`, `detail`, `new`

```` 
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->disable(Action::NEW, Action::DELETE)
            ->disable(Action::NEW, Action::EDIT)
        ;
    }
````



### Gestion du cycle de vie - Ecouter les évènements

#### Hachage du mot de passe lors de la création du nouvel utilisateur

Création d'un nouvel utilisateur à partir du Dashboard, j'utilise le souscripteurs d'événements il contient la liste des événements à écouter, `Easyadmin` fourni des écouteurs évènements avant et après la persistances des données dans une table à l’aide d’un ORM : Doctrine.

Envoi d'un mail automatique au nouvel utilisateur pour la validation du compte.

Création du dossier et de la classe `src/EventSubscriber/EasyAdminSubscriber.php`

````
namespace App\EventSubscriber;

use App\Entity\User;
use App\Security\EmailVerifier;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    private EmailVerifier $emailVerifier;
    protected $passwordEncrypt;

    public function __construct(UserPasswordHasherInterface $passwordEncrypt,EmailVerifier $emailVerifier)
    {
        $this->passwordEncrypt = $passwordEncrypt;
        $this->emailVerifier = $emailVerifier;
    }

    // 
    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['encodePasswordNewUser'],
            AfterEntityPersistedEvent::class =>  ['sendEmailAfterPersistNewUser'],
        ];
    }

    // hashage du mot de passe lors de la création d'un nouvel utilisateur dans le dashboard easyAdmin
    public function encodePasswordNewUser(BeforeEntityPersistedEvent $event)
    {
        $userEntityInstance = $event->getEntityInstance();
        // Ne retourne rien si ce n'est pas une instance de la classe User 
        if (!($userEntityInstance instanceof User)) {
            return;
        }
        // Hashage du mot de passe basé sur security.yaml config de la class $user
        $hashedPassword = $this->passwordEncrypt->hashPassword(
            $userEntityInstance,
            $userEntityInstance->getPassword()
        );
        // $hashedPassword = $this->passwordEncrypt->hashPassword($userEntityInstance->password);
        // Remplace le mot de passe en clair par celui crypté
        $userEntityInstance->setPassword($hashedPassword);
    }

    public function sendEmailAfterPersistNewUser(AfterEntityPersistedEvent $event) {
        $userEntityInstance = $event->getEntityInstance();
        // Ne retourne rien si ce n'est pas une instance de la classe User 
        if (!($userEntityInstance instanceof User)) {
            return;
        }

        // generate a signed url and email it to the user
        $this->emailVerifier->sendEmailConfirmation('app_verify_email', $userEntityInstance,
        (new TemplatedEmail())
            ->from(new Address('dwwm2021@gmail.com', 'Mail bot'))
            ->to($userEntityInstance->getEmail())
            ->subject('Veuillez confirmer votre email')
            ->htmlTemplate('registration/confirmation_email.html.twig')
        );
    }
````

TODO gerer l'activation par email bug token



### Afficher un titre dans la page détail

`EasyAdmin` récupère le retour de fonction `__ToString` défini dans l'entité concerné.



### Sécuriser l'accès aux l'urls `src/controller/Admin`

La désactivation d’une action  signifie qu’elle n’est pas affichée dans l’interface et que  l’utilisateur ne peut pas exécuter l’action même s’il *pirate* l’URL. S’ils essaient de le faire, ils verront une exception « Action interdite ».

````
   // src/controller/admin/edibilityCrudController.php
   
   // ...
   
   // restreindre les actions  
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // Ajoute à la page INDEX l'action DETAIL et desactive les liens vers éditions et suppresion
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::NEW, Action::DELETE)
            ->disable(Action::NEW, Action::EDIT)
        ;
    }
````



Je donne accès au Dashboard EasyAdmin via le fichier de sécurité de Symfony `config/package/security.yaml`

Si un utilisateur ayant le role `ROLE_USER` tente accéder a une page sécurisé en modifiant l'url il recevra réponse code erreur 403  "accès non autorisé" car easyAdmin crée un Jeton à partir de cette url et donc la signature ne sera pas valide.





### Configuration du menu de l'utilisateur connecté



## FRONT END - Affichage de la page profil

cree dossier CurrentUser + controlller CurrentUser

Template dossier + profil.html.twig qui extends esayadmin.

Un dossier ainsi qu'un fichier est généré

### Utiliser un filtre pour la gestion des date avec Twig

Le filtre `format_datetime` fait partie de `IntlExtension` qui n’est pas installé par défaut. Installez-le d’abord:

```
$ composer require twig/intl-extra
```

Ensuite, sur les projets Symfony, installez le `twig/extra-bundle`:

```
$ composer require twig/extra-bundle
```

Exemple dans un template Twig

````
<div class="text-nowrap">
    <div class="col mt-3">
        <p class="ftSize--small">
        <span class="fw-bold">Date d'inscription</span>
        <br>{{app.user.createdAt|format_datetime('full', 'none', timezone ='Europe/Paris', locale='fr')}}
    </p>
   </div>
</div>
````



### Développement des fonctions Twig personnalisées, elles permettent d'exécuter des actions dans les templates.

Lorsque l'utilisateur courant affiche sont profil, je souhaite qu'il puisse voir le temps écoulé depuis sont inscription.

### Installer une extension twig 

````
bin/console make:twig-extension
````

````
// src/Twig/TwigExtension.php

<?php

namespace App\Twig;
// ...

public function getFunctions(): array
    {
        return [
            // ...
            new TwigFunction('dateDiff', [$this, 'dateDiff']),
        ];
    }
    
    public function dateDiff(string $date){
    	// strtotime('now') renvoi un timestamp en secondes depuis le 1er Janvier 1970
    	// Converti au format date 
        $tomorrow = date("m/d/Y H:i:s", strtotime('now'));
        // Renvoie un objet DateTime pour la date courante
        $tomorrow = date_create($tomorrow);
        // Renvoie un objet DateTime pour de l'inscription de l'utilisateur
        $target = date_create($date);
        // renvoie la différence entre deux objets DateTime.
        $diff = $tomorrow->diff($target);

        $years = $diff->y;
        $month = $diff->m;
        $days = $diff->d;
        $hour = $diff->h;
        $min = $diff->i;
        $sec = $diff->s;

		// Initialise un tableau
        $resultat=array();
        // Ajoute chaque éléments à la fin du tableau avec la méthode array_push
        
        if($years >= 1){
            if($years == 1){
                array_push($resultat, $years." an"); 
            } else {
                array_push($resultat, $years." ans");
            }
        } 
        elseif( $month >= 1 ) {
            array_push($resultat, $month." mois");
        } 
        elseif( $days >= 1 ){
            if($days == 1){
                array_push($resultat, $days." jour"); 
             } else {
                array_push($resultat, $days." jours");
             }
        }
        elseif( $hour >= 1 ){
            if($hour == 1){
                array_push($resultat, $hour." heure"); 
             } else {
                array_push($resultat, $hour." heures");
             }
        }
        elseif( $min >= 1 ){
            if($min == 1){
                array_push($resultat, $min." minute"); 
             } else {
                array_push($resultat, $min." minutes");
             }
        }
        elseif( $sec >= 1 ){
            if($sec == 1){
                array_push($resultat, $sec." seconde"); 
             } else {
                array_push($resultat, $sec." secondes");
             }
        }
        
        $dateResult = implode(" ", $resultat);
        return $dateResult;
    }
    
````



### Personnalisation des formulaires avec mise en forme

````php
// scr/Form/registratioFormType.php

// ...
class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('pseudo',TextType::class,[
                'required' => true,
                'row_attr'=> ['class'=>'form-group'],
                'label' => 'Email',
                'attr'=> ['class'=> 'form-control'],
            ])
            ->add('email',null,[
                'required' => true,
                'row_attr'=> ['class'=>'form-group'],
                'label' => 'Email',
                'attr'=> ['class'=> 'form-control'],
            ])
	// ...
````



### Formulaire de contact

#### Création de l'entité Contact  à partir du diagramme de classe.

````
bin/console make :entity
````

#### Migrations des données

````
bin/console make:migration
bin/console make:doctrine:migrations/migrate
````

#### Création du controller

````bash
bin/console make:controller
````

#### Création du formulaire de contact

````bash
bin/console make:form
````

retour console

````bash

$ bin/console make:form

 The name of the form class (e.g. BraveGnomeType):
 > Contact

 The name of Entity or fully qualified model class name that the new form will be bound to (empty for none):
 > Contact
````

#### Configuration du formulaire de contact

````php
// src/Form/ContactFormType.php

<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lastname',TextType::class,[
                'row_attr' => ['class' => 'mb-3'],
                'label'=>false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder'=>'Nom *'
                ],
                'required' => true,
            ])
            ->add('firstname',TextType::class,[
                'row_attr' => ['class' => 'mb-3'],
                'label'=>false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder'=>'Prénom *'
                ],
                'required'   => true,
            ])
            ->add('email',EmailType::class,[
                'row_attr' => ['class' => 'mb-3'],
                'label'=>false,
                'label_attr'=>['class'=>''],
                'attr' => [
                    'class' => 'form-control',
                    'placeholder'=>'Adresse mail *'
                ],
                'required'   => true,
            ])
            ->add('phone',TelType::class,[
                'row_attr' => ['class' => 'mb-3'],
                'label'=>false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder'=>'Téléphone'
                ],
                'required'   => false,
            ])
            ->add('message',TextareaType::class,[
                'row_attr' => ['class' => 'mb-3'],
                'label'=>false,
                'attr' => [
                    'rows' => '5',
                    'class'=>'form-control mb-4',
                    'placeholder'=>'Laissez votre message ... *'
                ],
            ])
            ->add('save',SubmitType::class, [

                'label'=>'Envoyer',
                'attr' => ['class' => 'btn btn-success my-3'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
````

#### Config du Controller `ContactController.php`

Stocke l'adresse admin d'une variable global dans `config/service.yaml`

````yaml
parameters:
    / ...
    app.adminEmail: 'dwwm2021@gmail.com'
````

##### Editer le Controller `src/Controller/ContactController.php`

````php
<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactFormType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

class ContactController extends AbstractController
{
       /**
     * @Route("/contact", name="sendMail")
     */
    public function index(Request $request, MailerInterface $mailer)
    {
        $contact = new Contact();
        $formContact = $this->createForm(ContactFormType::class,$contact);
        $formContact->handleRequest($request);

        if( $formContact->isSubmitted() && $formContact->isValid() )
        {
            // peristence des données
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush(); 
            // Envoi de l'email
            $this->sendMail($formContact, $mailer);

            // redirection vers le forum
            return $this->redirectToRoute('app_mushrooms');
        }

        return $this->render('contact/index.html.twig', [
            'controller_name' => 'ContactController',
            'title' => 'Contact',
            'formContact' => $formContact->createView(),
        ]);
    }


    public function sendMail($formContact, $mailer)
    {
        $email = (new TemplatedEmail())
            ->from($formContact->get('email')->getData())
            ->to($this->getParameter('app.adminEmail'))
            ->subject('Le royaume des champignons - formulaire contact')
            ->htmlTemplate('contact/sendEmailForAdmin.html.twig')
            ->context([
                'sending_date' => new \DateTime('NOW'),
                'lastname' => $formContact->get('lastname')->getData(),
                'firstname' => $formContact->get('firstname')->getData(),
                'phone' => $formContact->get('phone')->getData(),
                'message' => $formContact->get('message')->getData(),
            ]);
            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {
                throw $this->createNotFoundException('Une erreur c\'est produite. Impossible d\'envoyer l\'émail. ');
            }
    }
}

````

#### Edition de la page contact `template/contact/index.html.twig`

````twig
{% extends 'base.html.twig' %}

{% block title %}Hello ContactController!{% endblock %}
{% block description %}{% endblock %}


{% block body %}
<div class="container contact">
    <div class="d-flex justify-content-center">
        <div class="col-12 col-md-8 rounded">
            <h3 class="text-center my-5">Formulaire de contact</h3>
            {{form(formContact)}}
        </div>
    </div>
</div>
{% endblock %}
````

#### Mise en forme du message pour l'envoi de l'email

##### Installer cssliner pour faciliter la prise en charge du css.

````
composer require twig/extra-bundle twig/cssinliner-extra
````

##### config le chemin d'accès au fichier de styles dans 

````yaml
# config/packages/twig.yaml

twig:
	# ...
    paths:
        '%kernel.project_dir%/public/build': styles
````

##### Personnaliser le mail

````twig
{# template/contact/sendEmailToAdmin.html.twig #}
<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-12 rounded border">
            <h1>Formulaire de contact</h1>
            <p>
                <span class="fw-bold">
                    Date d'envoi du formulaire : 
                </span>
                <span class="font-italic">
                   {{sending_date|format_datetime('full', 'short', timezone ='Europe/Paris', locale='fr')}}
                </span>
            </p>
            <p>
                <span class="fw-bold">
                    Envoyé par : 
                </span>
                {{firstname}} {{lastname}}
            </p>
            <p>
                <span class="fw-bold">
                    Coordonnées téléphonique : 
                </span>
                {{phone}}
            </p>
            <p>
                <span class="fw-bold">
                    Message : 
                </span>
                {{message|nl2br}}
            </p>
        </div>
    </div>
</div>
````

##### Intégré une image dans le mail

````yaml
# config/packages/twig.yaml
twig:
    # ...

    paths:
        # point this wherever your images live
        '%kernel.project_dir%/assets/images': images
````

````twig
{# '@images/' refers to the Twig namespace defined earlier #}

<img src="{{ email.image('@images/logo.png') }}" alt="Logo">

<h1>Welcome {{ email.toName }}!</h1>
{# ... #}
````



## FRONT END - SASS 

### fonction assombrir une couleur

````
// Atténue la couleur 
@function setTextColorIfHover($baseColor) {
    @return(darken($baseColor, 20%));
}

// Personnaliser les liens
@mixin link-color {
    $myLinkColor : white;
    a{
        text-decoration: none !important;
        
        &:link {
            color: $myLinkColor !important;
        }

        &:visited {
            color: white;
        }

        &:hover {
            color: setTextColorIfHover($myLinkColor);
        }
    }
}
````



## Réalisation du forum

#### Création de l'entité forum à partir du diagramme de classe.

````
bin/console make :entity
````

`ForumSubject`

`ForumCommentary`

`ForumCategory`

Création des relations :

- `User` &rarr;`OneToMany` &rarr;`ForumSubject`
- `User` &rarr;`OneToMany` `ForumCommentary`
- `ForumSubject`&rarr;`OneToMany` &rarr;`ForumCommentary`
- `ForumSubject`&rarr;`ManyToMany` `ForumCategory`

#### Migrations des données

````
bin/console make:migration
bin/console make:doctrine:migrations/migrate
````

#### Création du controller

````bash
bin/console make:controller
````

Création d'un formulaire pour ajouter un sujet en lien avec l'utilisateur

````

````

### BUNDLE -Ajouter l'éditeur de texte CkEditor

La 1ère commande à entrer dans le terminal va **récupérer FOSCKEditor** et l'**intégrer à votre projet Symfony** et au **dossier vendor**.

```shell
composer require friendsofsymfony/ckeditor-bundle
```

Shell

Il faudra ensuite **télécharger les ressources** nécessaires au fonctionnement de CKEditor (HTML, CSS, Javascript) et les installer.

```shell
php bin/console ckeditor:install
```

Shell

Ensuite, nous allons installer toutes les ressources nécessaires dans le dossier "**public**"

```shell
php bin/console assets:install public
```



#### Configurer FOSCKEditor

Défini l'emplacement de CKeditor  pour générer le rendu de l'élément dans les Templates,  j'ajoute les outils qui seront proposées aux utilisateurs.

````yaml
# config/package/fos_ckeditor.yaml

twig:
    form_themes:
        - '@FOSCKEditor/Form/ckeditor_widget.html.twig'

fos_ck_editor:
    # ...
    base_path: "build/ckeditor"
    js_path:   "build/ckeditor/ckeditor.js"
    configs:
        main_config:
            toolbar:
                - { name: "basicstyles", items: ['Bold', 'Italic', 'Underline', 'Strike', 'Blockquote', '-', 'Link''-','RemoveFormat'] },
                - { name: "paragraph", items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
                - { name: "insert", items: ['Image','Table','HorizontaleRule','SpecialChar'] },
                - { name: "styles", items: ['Format','Styles'] },
                - { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
                
````

Je dois gérer le rendu dans mes Templates et dans les interfaces CKeditor 



#### Configuration dans le CrudController d'easyAdmin

Mise en forme du champ pour la description d'une publication dans L'interface de EasyAdmin.

````
<?php

namespace App\Controller\Admin;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
// ...

// Configuration des options du CRUD : titre, pagination, format de date, texte des liens
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            // Active le rendu de l'élément CKeditor dans le formulaire 
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
          // ...
        ;
    }
    
    // ...
    public function configureFields(string $pageName): iterable
    {
        return [
        // ...
        // DESCRIPTION
                FormField::addPanel('Détail de la publication'),
                TextField::new('title','Titre du post')
                    ->setHelp('Ce champ est obligatoire.'),
                TextEditorField::new('description','Description du post')->setFormType(CKEditorType::class),
        // ...        
````

![CreationModifForumSujet](C:\workspace_symfony\PHOTO CODE RAPPORT\CreationModifForumSujet.JPG)

Pour gérer l'affiche pour une collection, relation OneToMany, je crée un formulaire car je souhaite 

````
    // ...
    public function configureFields(string $pageName): iterable
    {
        return [
                CollectionField::new('forumCommentary','Commentaires posté par les membres')
                    ->setEntryType(CkeditorForumCommentaryFormType::class)
                    ->hideOnIndex(),
     // ...
````





#### Création du formulaire

````bash
bin/console make:form
````

Si l'administrateur supprime un compte utilisateur avec easyAdmin 

EN ajoutant `onDelete="SET NULL"` pour la  clé étrangère user_id de l'entit

é `forumSubjects` et après avoir effectuer la migration vers la base de données, MySQL  bascule les champs lies a nulle ce qui supprime les relations avec  l'utilisateur. Les poste reste accessible sur le site web.

Idem pour les commentaires

````
// 
class ForumSubject
// ...
{
 /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="forumCommentarys")
     *  @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $user;
   // ...
````





## Fixtures Jeux de données

Installer bundle

````
composer require --dev orm-fixtures
````

Charger le jeu de données dans la base de données

````
php bin/console doctrine:fixtures:load
````

````
<?php

namespace App\DataFixtures;

use App\Entity\User;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    protected $passwordEncrypt;

    public function __construct(UserPasswordHasherInterface $passwordEncrypt)
    {
        $this->passwordEncrypt = $passwordEncrypt;
    }

    public function load(ObjectManager $manager): void
    {
       // UTILISATEURS
        $userAdmin = new User;
         // Hashage du mot de passe basé sur security.yaml config de la class $user
         $plaintextPassword = "123456";
         $hashedPassword = $this->passwordEncrypt->hashPassword(
            $userAdmin,
            $plaintextPassword
        );
        $userAdmin
        ->setFirstname("Eric")
        ->setLastname("Lanza")
        ->setEmail("lanzae32@gmail.com")
        ->setRoles(["ROLE_ADMIN"])
        ->setPassword($hashedPassword)
        ->setPseudo("Eric")
        ->setAvatarFilename("61a38e3cd8c77720759133.png")
        ->setIsVerified(true);

        $manager->persist($userAdmin);
        $manager->flush();
        // ...
````



## Configuration Dashboard admin





## Solution hébergement

J'ai choisi d'héberger mon application sur serveur virtuel privée ou VPS ( ce peut être conteneur ou une machine virtuelle) de chez OVH, le système d'exploitation est une distribution linux (ubuntu).

## Préparation de l'environnement de déploiement

Avant de déployer et d'exécuter l'application , je dois préparer le système d'exploitation.

Pour que mon application Symfony puis fonctionné je dois disposer d'un ensemble d'outils logiciels sur le serveur de développement et de production.

- **Docker**

- **Symfony 5**
- **PHP** version minimum  7.4.25 et quelques dépendances
- **MySQ**L server - SGBD - serveur de base de données relationnelles SQL gérer par un moteur **MyISAM** ou **InnoDB**(lire et écrire dans la BDD)
- **PhpMyAdmin** propose une interface graphique pour administrer les bases de données.
- **Composer** pour la gestion des dépendances coté back end
- **Npm** pour la gestion des dépendances coté front end



2 solutions 

- Configurer le système manuellement
  - installer les logiciels et les dépendances du projet.

- Utiliser docker afin d'empaquetez les logiciels dans des conteneurs



### Installation de WSL (sous système linux) et Docker Desktop sous Windows

Configurer un environnement de développement WSL

Vérifier votre numéro de version et de build avecla **touche Windows + R** , entrer la commande winver mettre à jor le système si besoin.(windows update)

Ouvrir l’interpréteur de commandes PowerShell.

````
wsl --install
````

 Lister vos distributions Linux installées et vérifier la version de WSL.

````
wsl -l -v
````

Rechercher les versions disponibles dans le  magasin en ligne.

````
wsl -l -o
````

Installer une version.

````
wsl --install -d <Distribution Name>
````

Configurer la version de wsl en entrant 1 ou 2 à la place de `<Version#>`.

````
wsl --set-default-version <Version#>
````

Mise à jour de WSL 1 à WSL 2 sur les distributions Linux précédemment installées.

````
wsl --set-version <distro name> 2
````

Mettre à jour WSL

```
powershell wsl --update
```

Installer Docker Desktop.



### Installation des conteneurs Docker

#### Définition d'un conteneur

un conteneur est unité logicielle standard qui regroupe le code et ses dépendances afin que l'application s'exécute rapidement et de manière fiable d'un environnement informatique à un autre. 

Une image de conteneur Docker est un package logiciel léger, autonome et exécutable qui comprend tout le nécessaire pour exécuter une application : code, environnement d'exécution, outils système, bibliothèques système et paramètres.



### Configuration des scripts d'installation

Docker-compose permet de gérer un ensemble de conteneurs (services)  Docker au sein d'un même fichier de configuration (fichier YAML). Cela  permet de gérer ce stack (cet ensemble de conteneurs, services) de  manières centralisée.

Je commence par créé un  dossier `docker` à la racine de mon projet, puis j'ajoute fichier `docker-compose.yml`,enfin

 je rédige le script des conteneurs

````
# docker-compose.yml
  
version: "3.3"
services: 

    db:
        image: mysql:5.7.37
        container_name: db_docker_symfony
        restart: always
        volumes:
            - db-data:/var/lib/mysql
        # ports:
        #     - 3307:3306
        environment:
            # MYSQL_ALLOW_EMPTY_PASSWORD: 'yes'
            MYSQL_ROOT_PASSWORD: Mysq&13004
            MYSQL_USER: eric
            MYSQL_PASSWORD: rc-mydev&8313
        networks:
            - dev

    phpmyadmin:
        image: phpmyadmin:5.1.1
        container_name: phpmyadmin_docker_symfony
        restart: always
        depends_on:
            - db
        ports:
            - 8081:80
        environment:
            PMA_HOST: db
        networks:
            - dev

    maildev:
        image: maildev/maildev
        container_name: maildev_docker_symfony
        command: bin/maildev --web 80 --smtp 25 --hide-extensions STARTTLS
        ports:
            - "8082:80"
        restart: always
        networks:
            - dev

    www:
        build: php
        container_name: www_docker_symfony
        ports:
            - "8080:80"
            - "443:443"
        volumes:
            - ./php/vhosts:/etc/apache2/sites-enabled
            - ../:/var/www/royaumedeschampignons
        restart: always
        networks:
            - dev

networks:
    dev:

volumes:
    db-data:
````



####   Exécuter les conteneurs

 Se placer dans le dossier `docker/` et exécuter la commande.

  ```
  docker-compose up -d
  ```

 ou

````
docker-compose start
````



Tester les url renseignées de la config dans le navigateur

Modification des droits d'accès 

````
sudo chown -R $USER ./
````



### Création et migration de la base de donnée

#### Configure dans le fichier `.env` l'accès à la base de donnée.

 Je paramètre le fichier de configuration `.env` pour accéder à la base de données en ajoutant le nom du conteneur MySQL que docker a démarré.

````
// .env

DATABASE_URL="mysql://root@db_docker_symfony/mycodb?serverVersion=5.7"
````

#### Création de la base de données

Pour lancer la commande de création de la base de données je dois me connecte au conteneur Symfony .

````
sudo docker exec -it www_PHP_Apache bash
````

#### Création de la base de données via une commande symfony

````
php bin/console doctrine:database:create
````

#### Effectuer la migration de données pour ajouter les tables dans la BDD

````
php bin/console doctrine:migrations:migrate
````

Je vérifie dans le navigateur `http://localhost:8081`



### Commandes Docker

#### Construction et recharger en une seule étape 

````
sudo docker-compose up --detach --build
````

#### Lister les conteneurs docker

````
docker images -a
````

#### Lister les containers docker-compose

````
sudo docker-compose ps
````

#### Lister les images docker-compose

````
sudo docker-compose images
````

#### Lister les volumes

````
sudo docker volume ls
````

#### Supprimer un volume

````
sudo docker volume rm <volume name>
````

#### Construire, détruire un conteneur

````
sudo docker-compose up -d

sudo docker-compose down
````

````
docker-compose build
````

#### Démarrer, stopper redémarrer tous les conteneurs

````
docker-compose start

docker-compose stop

docker-compose restart
````

Avec docker

````
docker stop $(docker ps -a -q)
````

#### Ce dois me connecte au conteneur Symfony .

````
sudo docker exec -it www_PHP_Apache bash
````

#### Supprimer une image docker stopper ou -f si actif

````
docker rmi <REPOSITORY>:<TAG>
````

````
docker rmi <IMAGE ID>
````

#### Supprimer tous les conteneurs  y compris ses volumes

````
docker rm -vf $(docker ps -a -q)
````

#### Tout supprimer container et images et volumes

 la reconstruction et le redémarrage ne suffisent pas, les conteneurs étant permanents, vous devez d'abord les supprimer 

````
sudo docker-compose stop
sudo docker-compose rm

sudo docker volume ls
sudo docker volume rm <nom volume>

sudo docker images -a
sudo docker rmi <REPOSITORY>:<TAG>
````

#### 

#### Vider et recharger le Cache (compilation Twig &rarr;PHP)

ATTENTION si APP_ENV= prod il faut vider et recharger le cache `php bin/console cache:clear` pour que les modification des Templates soit prise en compte.

##### Vider et recharger  défini par `APP_ENV` dans le fichier .env

```
php bin/console cache:clear
```

La commande s’exécutera dans l’environnement défini par `APP_ENV` variable d’environnement dans le fichier `.env`. Par défaut, `dev`. Nous pouvons utiliser l’option `--env` ou `-e` pour vider et réchauffer le cache pour l’environnement spécifié.

```
php bin/console cache:clear --env=prod
```

##### Vider le cache prod sans recharger

```
php bin/console cache:clear --no-warmup --env=prod
```

##### Reconstruire  le cache

````
php bin/console cache:warmup --env=prod
````



#### Générer les dépendances de bundle dans le dossier public

Installation du bundle easyadmin dans le dossier public 

````
php bin/console assets:install
````





### Activer le HTTPS avec Let's Encrypt

Aller dans le VPS entrer dans le conteneur 

````
sudo docker exec -it www_PHP_Apache bash
````



**Installer CertBot pour apache**

````
apt install certbot python3-certbot-apache
````

```bash
systemctl reload apache2
```



**Installer le certificat** pour `royaumechampignons.ddns.net`

```bash
sudo certbot --apache
```



**Le certificat est maintenant installé et chargé dans la configuration d’Apache.**

Les dossier crées par cerbot

````
 /etc/letsencrypt/
````

Dans le conteneur PHP on retrouve le fichier `vhosts.conf` pour http dans la config `docker-compose.yml`

````
volumes:
   . \- ./php/vhosts:/etc/apache2/sites-enabled
````

cerbot a créer les clés et le fichier  `etc/sites-enabled/vhosts-le-ssl.conf` pour l'accès https.



**Vérifier le statut de ce service** et vous assurer qu'il est actif et qu'il fonctionne

````
systemctl status certbot.timer
````



**Renouvellement automatique**

````
sudo docker exec -it www_PHP_Apache bash
````

````
certbot renew --dry-run
````

Commande a exécuter dans le conteneur docker.

Le package `certbot`  installé prend en charge les renouvellements en incluant un script de renouvellement dans `/etc/cron.d`, qui est géré par un service `systemctl` appelé `certbot.timer`. Ce script est exécuté deux fois par jour et renouvellera  automatiquement tout certificat qui se trouve dans une période de trente jours précédant son expiration.

Problème : la commande `systemctl` existe pas dans le conteneur.

Ajouter un volume pour conserver les certificat

#### Démarrer arrêter stopper ngnix ou apache2 (ne peux pas démarrer un service dans un conteneur docker)

````
sudo systemctl start nginx

sudo systemctl restart nginx

sudo systemctl stop nginx
````



### Déploiement  sur le VPS

Se connecter sur le vps via le terminal

Se rendre dans `eric/dev/cda-projetexamen` en tant que eric `su eric`

Première installation 

````
cd eric/dev
git clone https://gitlab.com/Eric-12/cda-projetexamen.git
cd /home/eric/dev/cda-projetexamen
sudo docker-compose up -d
sudo docker exec -it www_docker_symfony bash
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
composer install
php bin/console cache:clear
php bin/console cache:clear --env=prod
npm install
npm run build

````



Cloner le repos sinon mettre à jour

```
git branch
git pull origin main
```

````
npm install
composer installex
````



### L'automatisation avec GITLAB CI

 Construire le livrable de notre front

 délivrer ce dist/ sur notre VPS 



## CI/ CD - Intégration continue

Création d'une clef SSH sur votre VPS pour que le docker GITLAB puisse communiquer avec votre VPS

````
su eric
````

Création de la clé

````
ssh-keygen -t ed25519 -C "CI/CD"
````

Vérifier

````
cd /home/eric/.ssh
````

Ouvrir le fichier clé privée attention pas la clé publique `id_ed...pub`

````
sudo nano id_ed...
````

copier puis allez sur le repo dans setting &rarr; CI/CD &rarr;  variable

Ajouter la clé







construire  et déployer notre application sur un serveur de production

Mise en place de la pipeline.

On va déléguer  le travail exécution de la pipeline a un runner

Ajoute le fichier `.gitlab-ci.yml` à la racine du projet

On va utiliser l'image `jakzal/phpqa:php7.4`

stages = liste des étapes

puis 




     Let's encrypt for generate the certificates, use certbot
     && a2ensite royaumechampignons.ddns.net \
     && apt-get update \
     && apt-get install -y snapd \
     && snap install core \
     && snap refresh core \
     && snap install --classic certbot \
     && ln -s /snap/bin/certbot /usr/bin/certbot 
     && certbot certonly --apache -d royaumechampignons.ddns.net -m lanzae32@gmail.com --agree-tos --webroot-path /var/www/royaumedeschampignons/



## Tests 

### Cypress

**Installation** 

````
npm install cypress --save-dev
````

**Lancer**

````
npx cypress open
````

**Test page inscription**

Vérifie front et back (validator)

````
// inscription.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

describe('Inscription page', () => {
    beforeEach(() => {
        cy.visit('http://projetexamen-cda/register')
    })

    it('Vérifie le titre de la page inscription', () => {
        cy.get('h1').should('have.text', 'Inscription')
    })

    it('Affiche le formulaire de connexion', () => {
        cy.get('form').children().contains('Pseudo')
        cy.get('form').children().contains('Email')
        cy.get('form').children().contains('Mot de passe')
        cy.get('form').children().get('button').contains('Je m\'inscris')
    })

    it('Le formulaire est invalide si les champs sont vides', () => {
        cy.get('form').submit() 
        cy.get('form').children().contains('Veuillez entrer votre pseudo') 
        cy.get('form').children().contains('Veuillez entrer une adresse mail')
        cy.get('form').children().contains('Veuillez entrer un mot de passe')  
    })

    it('Le formulaire est invalide si le champ pseudo dépasse 12 caratères', () => {
        cy.get('form').children()
            .get('input[id="registration_form_pseudo"]').invoke('val', 'pseudoBeaucoupTropLong')
            .get('input[type="email"]').type('toto@gmail.com').should('have.value', 'toto@gmail.com')
            .get('input[type="password"]').type('Toto123456').should('have.value', 'Toto123456')
        cy.get('form').submit() 
        cy.get('form').children().contains('Votre pseudo ne dois pas contenir plus de 12 caractères.') 
    })

    it('Le formulaire est invalide si le champ email ne respecte pas le format', () => {
        cy.get('form').children()
            .get('input[id="registration_form_pseudo"]').type('newpseudo')
            .get('input[type="email"]').type('toto-gmail.com').should('have.value', 'toto-gmail.com')
            .get('input[type="password"]').type('toto').should('have.value', 'toto')
        cy.get('form').submit() 
        cy.get('form').children().contains('Huit caractères au minimum, au moins une lettre majuscule, une lettre minuscule et un chiffre') 
    })

    it('Le formulaire est invalide si le champ password ne respecte pas le format', () => {
        cy.get('form').children()
            .get('input[id="registration_form_pseudo"]').type('newpseudo')
            .get('input[type="email"]').type('toto2@test.com').should('have.value', 'toto2@test.com')
            .get('input[type="password"]').type('toto').should('have.value', 'toto')
        cy.get('form').submit() 
        cy.get('form').children().contains('Huit caractères au minimum, au moins une lettre majuscule, une lettre minuscule et un chiffre') 
    })

    it('Le formulaire est invalide si le compte existe', () => {
        cy.get('form').children()
            .get('input[id="registration_form_pseudo"]').type('newpseudo')
            .get('input[type="email"]').type('skipper@gmail.com').should('have.value', 'skipper@gmail.com')
            .get('input[type="password"]').type('Toto123456').should('have.value', 'Toto123456')
        cy.get('form').submit() 
        cy.get('form').children().contains('Il y a déjà un compte avec cet email') 
    })
````

### PHP UNIT

````
php bin/phpunit tests/Entity/UserEntityTest.php --testdox
````


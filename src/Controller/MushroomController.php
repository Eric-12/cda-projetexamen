<?php

namespace App\Controller;

use App\Entity\Mushroom;
use App\Form\SearchMushroomFormType;
use App\Repository\MushroomRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Knp\Component\Pager\PaginatorInterface; // Nous appelons le bundle KNP Paginator
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request; // Nous avons besoin d'accéder à la requête pour obtenir le numéro de page

class MushroomController extends AbstractController
{
    /**
    * @Route("/guide-des-champignons/description/{slug}", name="app_mushroom_detail", methods={"GET"})
    */
    public function description(string $slug, MushroomRepository $mushroomRepository) 
    {
        $descriptionMushroom = $mushroomRepository->findOneBy( ['slug' => $slug] );
        if($descriptionMushroom ==null){
            Throw $this->createNotFoundException();
        }

        return $this->render('mushrooms/descriptionMushroom.html.twig', [
            'controller_name'     => 'MushroomController',
            'title'               => 'Description',
            'descriptionMushroom' => $descriptionMushroom,
        ]);
    }

    /**
     * @Route("/guide-des-champignons/{page}/{search}", 
     *          name="app_mushrooms",
     *          methods={"GET","POST"}
     *        )
     */
    public function mushrooms ( MushroomRepository $mushroomRepository, 
                                PaginatorInterface $paginator, 
                                Request $request,
                                int $page = 1,
                                string $search =''): Response
    {
        // si l'utilisateur effectuer une recherche par nom commun
        $searchMushroomForm = $this->createForm(SearchMushroomFormType::class);
        $searchMushroomForm->handleRequest($request);

        // si l'utilisateur valide la recherche par nom commun
        if( $searchMushroomForm->isSubmitted() && $searchMushroomForm->isValid() )
        {
            // getData() contient la valeur soumises dans le formulaire
            $search = $searchMushroomForm->getData();
            // $search = $searchMushroomForm->get('name')->getData();
            // reinitialise la pignation
            $request->query->set('page', 1);
            // redirge sur la meme route avec un nouveau paramètre de recherche
            return $this->redirectToRoute('app_mushrooms',['search' => $search['name'] ]);
        }

        if($search == null) {
            $listMushrooms = $mushroomRepository->findBy(['visibility' => true],['commonname' => 'ASC']);
        } else {
            $listMushrooms = $mushroomRepository->findByStartcommonName($search);
            if(count($listMushrooms) == 0){
                $listMushrooms = $mushroomRepository->findBy(['visibility' => true],['commonname' => 'ASC']);
            }
        }

        $listMushroomsPaginator = $paginator->paginate(
            $listMushrooms, // Requête contenant les données à paginer.
            $page,          // $request->query->getInt('page', 1), 
            10              // Nombre de résultats par page.
        );

        // PERSONALISER POUR LE RENDU
        $listMushroomsPaginator->setCustomParameters([
            'align'   => 'center',
            'size'    => 'medium',
            'rounded' => false,
        ]);

        // Rendu de la page 
        return $this->render('mushrooms/mushrooms.html.twig', [
            'controller_name'        => 'MushroomsController',
            'title'                  => 'Guide des champignons',
            'listMushroomsPaginator' => $listMushroomsPaginator,
            'searchMushroomForm'     => $searchMushroomForm->createView(),
        ]);
    }

}

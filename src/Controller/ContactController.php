<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactFormType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="sendMail")
     */
    public function index(Request $request, MailerInterface $mailer): Response
    {
        $contact = new Contact();
        $formContact = $this->createForm(ContactFormType::class, $contact);
        $formContact->handleRequest($request);

        if( $formContact->isSubmitted() && $formContact->isValid() )
        {
            // peristence des données
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush(); 
            // Envoi de l'email
            $this->sendMail($formContact, $mailer);

            // redirection vers le forum
            return $this->redirectToRoute('app_mushrooms');
        }

        return $this->render('contact/index.html.twig', [
            'controller_name' => 'ContactController',
            'title' => 'Contact',
            'formContact' => $formContact->createView(),
        ]);
    }

    public function sendMail($formContact, $mailer)
    {
        $email = (new TemplatedEmail())
            ->from($formContact->get('email')->getData())
            ->to($this->getParameter('app.adminEmail'))
            ->subject('Le royaume des champignons - formulaire contact')
            ->htmlTemplate('contact/sendEmailToAdmin.html.twig')
            ->context([
                'sending_date' => new \DateTime('NOW'),
                'lastname' => $formContact->get('lastname')->getData(),
                'firstname' => $formContact->get('firstname')->getData(),
                'phone' => $formContact->get('phone')->getData(),
                'message' => $formContact->get('message')->getData(),
            ]);
            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {
                throw $this->createNotFoundException('Une erreur c\'est produite. Impossible d\'envoyer l\'émail. ');
            }
    }
}

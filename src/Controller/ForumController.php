<?php

namespace App\Controller;

use App\Entity\ForumCategory;
use App\Entity\ForumSubject;
use App\Entity\ForumCommentary;
use App\Form\SubjectsFormType;
use App\Form\CommentaryFormType;
use App\Form\ForumCategoryFormType;
use App\Repository\ForumSubjectRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Knp\Component\Pager\PaginatorInterface; // Nous appelons le bundle KNP Paginator

class ForumController extends AbstractController
{
    /**
     * @Route("forum/posts/{categorySlug}/{page}", name="forum_subjects")
     */
    public function posts(Request $request, 
                        PaginatorInterface $paginator, 
                        ForumSubjectRepository $forumSubjectRepository,
                        $categorySlug = "toutes-categories", 
                        int $page = 1
                        ): Response
    {
       
        // FORULAIRE NOUVEAU POST
        $newSubject = new ForumSubject();
        $form = $this->createForm(SubjectsFormType::class, $newSubject);
        $form->handleRequest($request);
        // Traitement du formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            $currentUser = $this->getUser();
            $newSubject->setUser($currentUser);
            $newSubject->setUserPseudo($currentUser->getPseudo());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newSubject);
            // On ajoute le nouveau sujet dans BD
            $entityManager->flush();
            return $this->redirectToRoute('forum_subjects');
        }

       
        // FORULAIRE RECHERCHE PAR CATEGORIE
        $formCategory = $this->createForm(ForumCategoryFormType::class);
        $formCategory->handleRequest($request);
        // Traitement du formulaire
        if ($formCategory->isSubmitted() && $formCategory->isValid()) {
            // récupère le slug categorie
            $categorySlug = $formCategory->get('name')->getData()->getSlug();
            // Réinitialise le numero de page
            $page = 1;
            return $this->redirectToRoute('forum_subjects', [ 'categorySlug' => $categorySlug ]);
        }


        // REQUETES   
        $nameCategory = null;  
        if($categorySlug === "toutes-categories"){
            // requete: recupere tous les sujets du forum
             $listSubjects = $this->getDoctrine()->getRepository(ForumSubject::class)->findBy([], ['createdAt' => 'DESC']);
             // Nombre de sujets
             $countSubjects = count($listSubjects);
             // nom de la catégorie
        } else {
            // requete: recupere tous les sujets d'une catégorie
            $listSubjects = $forumSubjectRepository->findByCategory($categorySlug);
            // Nombre de sujets dans cette catégory
            $countSubjects = count($listSubjects);
            if( $countSubjects > 0) {
                // requete : nom de la catégorie
                $nameCategory = $this->getDoctrine()->getRepository(ForumCategory::class)->findOneBy(['slug' => $categorySlug])->getName();
            } else {
               if($page > 1 ){
                   // si numéro de page supérieur 
                    Throw $this->createNotFoundException();
               } 
            }
        }

        // PAGINATION
        $subjectsPagination = $paginator->paginate(
            $listSubjects, // Requête contenant les données à paginer
            $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            10 // Nombre de résultats par page
        );

        $subjectsPagination->setCustomParameters([
            'align' => 'center',
            'size' => 'medium',
            'rounded' => true,
        ]);

        // RENDU TEMPLATE
        return $this->render('forum/subjects.html.twig', [
            'controller_name'    => 'ForumController',
            'title'              => 'Bienvenue sur le forum',
            'subjectsPagination' => $subjectsPagination,
            'subjectsform'       => $form->createView(),
            'formCategory'       => $formCategory->createView(),
            'memoryPage'         => $page,
            'categorySlug'       => $categorySlug,
            'countSubjects'      => $countSubjects,
            'nameCategory'       => $nameCategory,
        ]);
    }


    /**
     * @Route( "/post/{categorySlug}/{slug}/{memoryPage}/{page}",
     *      name="forum_subject"
     * )
     */
    public function post($categorySlug, 
                        string $slug, 
                        Request $request, 
                        PaginatorInterface $paginator,
                        $memoryPage, 
                        int $page = 1): Response 
    {
        // On récupère le sujet correspondant au slug
        $forumSubject = $this->getDoctrine()->getRepository(ForumSubject::class)->findOneBy(['slug' => $slug]);
        // récupère les commentaires laissé par les autres membres
        //$commentarys = $forumSubject->getForumCommentary();
        // OU
        $commentarys = $this->getDoctrine()->getRepository(ForumCommentary::class)
            ->findBy(['forumSubject' => $forumSubject->getId()], ['createdAt' => 'DESC']);

        $commentsPagination = $paginator->paginate(
            $commentarys,   // Résultat de la requête contenant les données à paginer
            $page,          // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            10              // Nombre de résultats par page
        );

        $commentsPagination->setCustomParameters([
            'align' => 'center',
            'size' => 'medium',
            'rounded' => true,
        ]);

        // Formulaire pour l'ajout d'un nouveau commentaire
        $commentary = new ForumCommentary();
        $formComments = $this->createForm(CommentaryFormType::class, $commentary);
        $formComments->handleRequest($request);
        
        // Si le formulaire est validé 
        if ($formComments->isSubmitted() && $formComments->isValid()) {
             // Utilisateur avec session active
            $currentUser = $this->getUser();
            $commentary->setUser($currentUser);
            $commentary->setForumSubject($forumSubject);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($commentary);
            $entityManager->flush();
            return $this->redirectToRoute('forum_subjects',array('categorySlug'=> $categorySlug, 'slug' => $slug));
        }

        // Transmettre les données au modèle avec la méthode render
        return $this->render('forum/subject.html.twig', [
            'controller_name' => 'ForumController',
            'title' => $forumSubject-> getTitle(),
            'subject' => $forumSubject,
            'commentsPagination' => $commentsPagination,
            'commentsForm' => $formComments->createView(),
            'categorySlug' => $categorySlug,
            'memoryPage' => $memoryPage,
        ]);
    }   
}

<?php

namespace App\Controller\Admin;

use App\Entity\ForumCommentary;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ForumCommentaryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ForumCommentary::class;
    }

    // Configuration des options du CRUD : titre, pagination, format de date, texte des liens
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ->setEntityLabelInPlural('FORUM - Les commentaires')
            ->setEntityLabelInSingular('un commentaire')
            ->setEntityPermission('ROLE_ADMIN')
            // ->setDateIntervalFormat('%%y Year(s) %%m Month(s) %%d Day(s)')
            ->setTimezone('Europe/Paris')
            ->setPaginatorPageSize(15)
            ->setDefaultSort(['id' => 'ASC']);
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('id')->hideOnForm(),

            // DATE apparait seulement dans les détails
            // date création et update sont automatiquement ajouté avant la persistence des données
            FormField::addPanel('Date')
                ->onlyOnDetail(),
            DateTimeField::new('createdAt', 'Créé le')
                ->onlyOnDetail(),

            // DESCRIPTION
            FormField::addPanel('Détail'),
            TextEditorField::new('commentary', 'Commentaire')->setFormType(CKEditorType::class),

            AssociationField::new('user', 'Auteur du sujet'),

            AssociationField::new('forumSubject', 'Associé au sujet'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
        ;
    }

}

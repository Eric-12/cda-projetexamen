<?php

namespace App\Controller\Admin;

use App\Entity\Media;

use App\Controller\Admin\Field\VichImageField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class MediaCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Media::class;
    }

    // Configuration des options du CRUD : titre, pagination, format de date, texte des liens
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
        ->setEntityLabelInPlural('Les photos')
        ->setEntityLabelInSingular('une photo')
        ->setEntityPermission('ROLE_ADMIN')
            // ->setDateIntervalFormat('%%y Year(s) %%m Month(s) %%d Day(s)')
            ->setTimezone('Europe/Paris')
            ->setPaginatorPageSize(15)
        ;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            // FormField::addPanel('liste des médias'),
            IntegerField::new('id')->hideOnForm(),

             // DATE apparait seulement dans les détails
            // date création et update sont automatiquement ajouté avant la persistence des données
            DateTimeField::new('createdAt', 'Créé le')
                ->onlyOnDetail(),
            DateTimeField::new('updatedAt', 'Modifié le')
                ->onlyOnDetail(),

            // PHOTOS
            TextField::new('path', 'Nom du fichier')->hideOnForm(),
            ImageField::new('path', 'Photographies')
                ->setBasePath($this->getParameter('app.path.mushroom_images'))
                ->hideOnForm(),
            VichImageField::new('imageFile', 'Photo')
                ->onlyOnForms(), 
            AssociationField::new('mushroom'),

            // SlugField::new('slug')->setTargetFieldName('name')->hideOnForm()
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }

}

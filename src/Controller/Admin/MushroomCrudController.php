<?php

namespace App\Controller\Admin;

use App\Entity\Mushroom;
use App\Form\ImagesType;
use App\Form\LocalNameFormType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
// Config Action (index edit delete detail)
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
// pour afficher et modifier supprimer des éléménts de la collection media
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class MushroomCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Mushroom::class;
    }

    // Configuration des options du CRUD : titre, pagination, format de date, texte des liens
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Les champignons')
            ->setEntityLabelInSingular('une fiche descriptive')
            ->setEntityPermission('ROLE_ADMIN')
            // ->setDateIntervalFormat('%%y Year(s) %%m Month(s) %%d Day(s)')
            ->setTimezone('Europe/Paris')
            ->setPaginatorPageSize(15)
            ->setDefaultSort(['commonname' => 'ASC']);
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('id')->hideOnForm(),
            // DATE apparait seulement dans les détails
            // date création et update sont automatiquement ajouté avant la persistence des données
            DateTimeField::new('createdAt', 'Créé le')
                ->onlyOnDetail()
            ,
            DateTimeField::new('updatedAt', 'Modifié le')
                ->onlyOnDetail()
            ,

            // DESCRIPTION
            FormField::addPanel('Dénomination'),
            TextField::new('commonname', 'Nom commun')
                ->setHelp('Ce champ est obligatoire.')
            ,
            TextField::new('latinname', 'nom scientifique')
                ->hideOnIndex()
            ,
            CollectionField::new('localname', 'Nom locale')
                ->setEntryType(LocalNameFormType::class)
                ->hideOnIndex()
            ,

            // DOCUMENTATION VISIBLE
            FormField::addPanel('Espace public')
                ->setHelp('Fiche descriptive de l\'espèce visible sur le site web.')
                ->setCssClass('fw-bold')
            ,
            BooleanField::new('visibility', 'Visible'),

            // PHOTOS
            FormField::addPanel('Photographies'),
            AssociationField::new('medias', 'Nombre de photos')
                ->hideOnForm()
            ,
            CollectionField::new('medias', 'Vos photos')
                ->setEntryType(ImagesType::class)
                ->hideOnIndex()
            ,

            // CARACTERISTIQUES
            FormField::addPanel('Caractéristiques'),
            TextareaField::new('hat', 'Chapeau')
                ->hideOnIndex()
            ,
            AssociationField::new('lamellatype', 'Type de lamelle')->hideOnIndex(),
            TextareaField::new('lamella', 'Lamelle')
                ->hideOnIndex()
            ,
            TextareaField::new('foot', 'Pied')
                ->hideOnIndex()
            ,
            TextareaField::new('flesh', 'Chair')
                ->hideOnIndex()
            ,
            TextareaField::new('habitat', 'Habitat')
                ->hideOnIndex()
            ,
            TextareaField::new('comment', 'Commentaire')
                ->hideOnIndex()
            ,
            AssociationField::new('edibility', 'Comestible')
                ->hideOnIndex()
            ,
            TextField::new('slug')
                ->hideOnForm()
            ,
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->setPermission(Action::NEW, 'ROLE_ADMIN');
    }

}

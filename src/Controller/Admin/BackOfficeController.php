<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Media;
use App\Entity\Mushroom;
use App\Entity\Edibility;
use App\Entity\Localname;
use App\Entity\Lamellatype;
use App\Entity\ForumSubject;
use App\Entity\ForumCommentary;
use App\Entity\ForumCategory;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use Symfony\Component\Security\Core\User\UserInterface;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class BackOfficeController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        // // return parent::index();
        $routeBuilder = $this->get(AdminUrlGenerator::class);
        return $this->redirect($routeBuilder->setController(MushroomCrudController::class)->generateUrl());
        
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Le royaume des champignons')
            ->setFaviconPath('build/images/icones/mushroomLogo.png')
            ->setFaviconPath('build/images/icones/logo.png');
    }

    public function configureMenuItems(): iterable
    {
        // Si non authentifé redirect vers la page login
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // Permissions d'accès accordés à l'administrateur
        if ($this->isGranted('ROLE_ADMIN')) {
            yield MenuItem::section('Utilisateurs');
                yield MenuItem::linkToCrud('Les membres', 'fas fa-users', User::class);

            yield MenuItem::section('Mycologie');
                yield MenuItem::linkToCrud('Guides champignons', 'fas fa-book-reader', Mushroom::class);
                yield MenuItem::subMenu('Les tables associées', 'fa fa-article')->setSubItems([
                    MenuItem::linkToCrud('Photographies', 'fas fa-camera', Media::class),
                    // ->setController(MediaCrudController::class),
                    MenuItem::linkToCrud('Consommation', 'fas fa-utensils', Edibility::class),
                    MenuItem::linkToCrud('Hyménium', 'far fa-eye', Lamellatype::class),
                    MenuItem::linkToCrud('Nom local', 'far fa-flag', Localname::class),
                ]);

                yield MenuItem::section('Forum');
                    yield MenuItem::linkToCrud('Espace de discussion', 'far fa-comment-dots', ForumSubject::class);
                    yield MenuItem::subMenu('Les tables associées', 'fa fa-article')->setSubItems([
                        MenuItem::linkToCrud('Les commentaires', 'far fa-comments', ForumCommentary::class),
                        MenuItem::linkToCrud('Les catégories', 'fas fa-table', ForumCategory::class),
                    ]);
        }

        yield MenuItem::section('Espace personnel');
            yield MenuItem::linkToRoute('Espace personnel', 'fa fa-id-card', 'app_currentUser_profil');

        yield MenuItem::section('Page du site');
            yield MenuItem::linkToRoute('Accueil', 'fa fa-home', 'app_home');
            // yield MenuItem::linkToRoute('Guide des champignons', 'fas fa-book-reader', 'app_mushrooms');
            // yield MenuItem::linkToRoute('Forum', 'far fa-comments', 'forum_subjects');
            // yield MenuItem::linkToRoute('Blog', 'fab fa-blogger', '');
            // Renvoie à une URL via un nom de la route
            // MenuItem::linkToRoute('Retour à l\'Accueil', 'fas fa-undo','app_home'),
            // Renvoie à une URL relative ou absolue (si pas de class alors spécifier que la valeur est null)
            // MenuItem::linkToUrl('Accueil', 'fas fa-undo','home'),
            //Renvoie  l'URL que l'utilisateur doit visiter pour se déconnecter de l'application
            // Definit par la propritété firewall du fichier de config security
            yield MenuItem::section('Connexion');
            yield MenuItem::linkToLogout('Me déconnecter', 'fas fa-sign-out-alt');
    }

    // Configuration du menu de l'utilisateur connecté
    public function configureUserMenu(UserInterface $user): UserMenu
    {       
        $currentEmail = $user->getUserIdentifier();
        $currentUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $currentEmail]);
        $avatar = $currentUser->getAvatarFilename();

        return parent::configureUserMenu($user)
            // Renvoi le nom de l'utilisateur
            ->setName($user->getUserIdentifier())
            // Afficher le nom de l'utilisateur à partir de la methode __toString()
            ->displayUserName(true)

            // Avvatar à partir d'une URL
            // ->setAvatarUrl('https://...')
            ->setAvatarUrl($this->getParameter('app.path.user_images')."/".$avatar)
            // Methode pour afficher l'avatar
            ->displayUserAvatar(false)
            // you can also pass an email address to use gravatar's service
            // ->setGravatarEmail($user->getEmail())

            // Ajouter un menu
            ->addMenuItems([             
                MenuItem::linkToRoute('Espace personnel', 'fa fa-id-card', 'app_currentUser_profil'),
                MenuItem::section(),
                MenuItem::linkToRoute('Accueil', 'fa fa-home', 'app_home'),
            ]);
    }

    /**
     * @Route("/current/user", name="current_user")
     */
    public function CurrentUser(): Response
    {
        return $this->render('current_user/profil.html.twig', [
            'controller_name' => 'CurrentUserController',
        ]);
    }

}

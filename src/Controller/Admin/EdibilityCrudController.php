<?php

namespace App\Controller\Admin;

use App\Entity\Edibility;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use function PHPUnit\Framework\containsOnlyInstancesOf;

class EdibilityCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Edibility::class;
    }

    // Configuration des options du CRUD : titre, pagination, format de date, texte des liens
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Consommation')
            ->setEntityLabelInSingular('une fiche descriptive')
            ->setEntityPermission('ROLE_ADMIN')
            // ->setDateIntervalFormat('%%y Year(s) %%m Month(s) %%d Day(s)')
            ->setTimezone('Europe/Paris')
            ->setPaginatorPageSize(15)
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('id')
                ->hideOnForm(),
            TextField::new('name', 'Nom'),
            TextField::new('path', 'Répertoire de destination'),

            // NOMBRE DE FICHES DESCRIPTIVES ASSOCIEES
            AssociationField::new('mushrooms', 'Nombre de fiches descriptives')
                ->hideOnForm()
            ,
            // COLLECTION 
            CollectionField::new('mushrooms', 'Liste des champignons')
                ->onlyOnDetail()
            ,

            // SLUG
            TextField::new('slug')
                ->hideOnForm(),
        ];
    }

    // restreindre les actions  
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // Ajoute à la page INDEX l'action DETAIL et desactive les liens vers éditions et suppresion
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::NEW, Action::EDIT, Action::DELETE)
        ;
    }

}

<?php

namespace App\Controller\Admin;

use App\Entity\ForumSubject;
use App\Form\CkeditorForumCommentaryFormType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ForumSubjectCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ForumSubject::class;
    }

    // Configuration des options du CRUD : titre, pagination, format de date, texte des liens
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            // Active le rendu de l'élément CKeditor dans le formulaire
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ->setEntityLabelInPlural('FORUM - Les sujets de conversations')
            ->setEntityLabelInSingular('un sujet')
            ->setEntityPermission('ROLE_ADMIN')
            // ->setDateIntervalFormat('%%y Year(s) %%m Month(s) %%d Day(s)')
            ->setTimezone('Europe/Paris')
            ->setPaginatorPageSize(15)
            ->setDefaultSort(['createdAt' => 'DESC']);
        ;
    }

 
    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('id')->hideOnForm(),

            // DATE apparait seulement dans les détails
            // date création et update sont automatiquement ajouté avant la persistence des données
            FormField::addPanel('Date')
                ->onlyOnDetail(),
            DateTimeField::new('createdAt', 'Créé le')
                ->onlyOnDetail(),
            DateTimeField::new('updatedAt', 'Modifié le')
                ->onlyOnDetail(),

            FormField::addPanel('Auteur du post'),
                AssociationField::new('user', false),

                // DESCRIPTION
                FormField::addPanel('Détail de la publication'),
                // CATEGORIES
                AssociationField::new('forumCategory', 'Catégories'),

                // TITRE
                TextField::new('title', 'Titre du post')
                    ->setHelp('Ce champ est obligatoire.'),

                // DESCRIPTION
                TextEditorField::new('description', 'Description du post')->setFormType(CKEditorType::class),

                // COMMENTAIRES
                AssociationField::new('forumCommentary', 'Nombre de commentaires')
                    ->hideOnForm()
                ,
                CollectionField::new('forumCommentary', 'Commentaires posté par les membres')
                    ->setEntryType(CkeditorForumCommentaryFormType::class)
                    ->hideOnIndex()
                ,
        ];
    }
 
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
        ;
    }
}

<?php

namespace App\Controller\Admin;

use App\Entity\User;
// Action page CRUD
use App\Form\CkeditorForumSubjectFormType;
// Config Action (index edit delete detail)
use App\Form\CkeditorForumCommentaryFormType;
use App\Controller\Admin\Field\VichImageField;
// Type de champ
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
// Photo
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    // Configuration des options du CRUD : titre, pagination, format de date, texte des liens
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
        ->setEntityLabelInPlural('Listes des utilisateurs du site')
        ->setEntityLabelInSingular('un nouvel utilisateur')
        // ->setEntityPermission('ROLE_ADMIN')
        // ->setDateIntervalFormat('%%y Year(s) %%m Month(s) %%d Day(s)')
        ->setTimezone('Europe/Paris')
        ->setPaginatorPageSize(15)
        ->setDefaultSort(['pseudo' => 'ASC']);
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        // getUserIdentifier
        return [
            // IntegerField pour ne pas modifier les ids
            IntegerField::new('id')->hideOnForm(),

            // DATE apparait seulement dans les détails
            // date création et update sont automatiquement ajouté avant la persistence des données
            DateTimeField::new('createdAt', 'Créé le')
                ->onlyOnDetail(),
            DateTimeField::new('updatedAt', 'Modifié le')
                ->onlyOnDetail(),

            // AVATAR
            ImageField::new('avatarFilename', 'Avatar')
            ->setBasePath($this->getParameter('app.path.user_images'))
            ->hideOnForm(),
            VichImageField::new('avatarFile', 'Avatar')
            ->onlyOnForms(),

            // Infos utilisateurs
            TextField::new('lastname', 'Nom')
                ->hideOnIndex(),
            TextField::new('firstname', 'Prénom')
                ->hideOnIndex(),
            TextField::new('pseudo', 'Pseudo'),
            EmailField::new('email', 'Email'),
            TextField::new('password', 'Mot de passe')
                ->hideOnIndex()
                ->setPermission(('ROLE_ADMIN')),
            ChoiceField::new('roles', 'Rôle')
                ->setChoices(
                [
                    'ROLE_ADMIN'  => 'ROLE_ADMIN',
                    'ROLE_EDITOR' => 'ROLE_EDITOR',
                    'ROLE_USER'   => 'ROLE_USER',
                ])
                ->allowMultipleChoices()
                ->setPermission(('ROLE_ADMIN')),
            BooleanField::new('isVerified', 'Compte activé')
                ->setPermission(('ROLE_ADMIN')),
            
            // Collection de Sujets publié sur le forum
            CollectionField::new('forumSubjects', 'Liste des questions posté sur le forum')
                ->setEntryType(CkeditorForumSubjectFormType::class)
                ->hideOnIndex()->hideOnDetail(),

            // Collection de commentaires publié sur le forum
            CollectionField::new('forumCommentarys', 'Liste des réponses publié sur le forum')
                ->setEntryType(CkeditorForumCommentaryFormType::class)
                ->hideOnIndex()->hideOnDetail(),
            
            TextField::new('slug')
                ->hideOnForm(),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }
}

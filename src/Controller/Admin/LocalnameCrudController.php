<?php

namespace App\Controller\Admin;

use App\Entity\Localname;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class LocalnameCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Localname::class;
    }

    // Configuration des options du CRUD : titre, pagination, format de date, texte des liens
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
        ->setEntityLabelInPlural('Nom local')
        ->setEntityLabelInSingular('le nom local')
        ->setEntityPermission('ROLE_ADMIN')
            // ->setDateIntervalFormat('%%y Year(s) %%m Month(s) %%d Day(s)')
            ->setTimezone('Europe/Paris')
            ->setPaginatorPageSize(15)
            ->setDefaultSort(['name' => 'ASC']);
        ;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('id')->hideOnForm(),

            // DATE apparait seulement dans les détails
            // date création et update sont automatiquement ajouté avant la persistence des données
            DateTimeField::new('createdAt', 'Créé le')
                ->onlyOnDetail(),

            TextField::new('name', 'Nom')
                ->setHelp('Saisir le nom en minuscule'),

            TextField::new('slug')
                ->hideOnForm(),
                
            AssociationField::new('mushroom', 'Listes des champignons'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }
}

<?php

namespace App\Controller\Admin;

use App\Entity\Lamellatype;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class LamellatypeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Lamellatype::class;
    }

    // Configuration des options du CRUD : titre, pagination, format de date, texte des liens
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Hyménium (Lames - Tubes - Plis - Aiguillons - Pores)')
            ->setEntityLabelInSingular('une fiche descriptive')
            ->setEntityPermission('ROLE_ADMIN')
            // ->setDateIntervalFormat('%%y Year(s) %%m Month(s) %%d Day(s)')
            ->setTimezone('Europe/Paris')
            ->setPaginatorPageSize(15)
        ;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('id')
                ->hideOnForm(),
            TextField::new('name', 'Nom'),
            TextField::new('path', 'Répertoire de destination'),
            CollectionField::new('mushrooms', 'Liste des champignons'),
            TextField::new('slug')
                ->hideOnForm(),
        ];
    }

    // restreindre les actions  
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // Ajoute à la page INDEX le lien vers l'action DETAIL
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            // Désactive les liens vers UPDATE et DELETE
            ->disable(Action::NEW, Action::DELETE, Action::EDIT)
            ;
    }

}

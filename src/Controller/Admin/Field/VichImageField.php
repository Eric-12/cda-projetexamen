<?php

namespace App\Controller\Admin\Field;

use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;
use Vich\UploaderBundle\Form\Type\VichImageType;


class VichImageField implements FieldInterface
{
    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null): self
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setFormType(VichImageType::class)
            ->setFormTypeOptions(['allow_delete' => true])
            // Affiche l'option pour la suppression
            ->setTranslationParameters(['form.label.delete' => 'tester'])
            ->setCustomOption('download_uri', false)
            ;
    }

    // public function setImageUri($imageUri): self
    // {
    //     $this->setCustomOption('image_uri', $imageUri);

    //     return $this;
    // }

    // public function setDownloadUri($downloadUri): self
    // {
    //     $this->setCustomOption('download_uri', $downloadUri);

    //     return $this;
    // }
}
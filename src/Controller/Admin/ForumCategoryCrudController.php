<?php

namespace App\Controller\Admin;

use App\Entity\ForumCategory;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ForumCategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ForumCategory::class;
    }

    // Configuration des options du CRUD : titre, pagination, format de date, texte des liens
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Catégorie')
            ->setEntityLabelInSingular('une catégorie')
            ->setEntityPermission('ROLE_ADMIN')
            // ->setDateIntervalFormat('%%y Year(s) %%m Month(s) %%d Day(s)')
            ->setTimezone('Europe/Paris')
            ->setPaginatorPageSize(15)
            ->setDefaultSort(['name' => 'ASC']);
        ;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('id')->hideOnForm(),
            // DATE apparait seulement dans les détails
            // date création et update sont automatiquement ajouté avant la persistence des données
            DateTimeField::new('createdAt', 'Créé le')
                ->onlyOnDetail(),
            DateTimeField::new('updatedAt', 'Modifié le')
                ->onlyOnDetail(),
            TextField::new('name', 'Nom'),
            TextField::new('slug', 'Slug')->hideOnForm(),
            AssociationField::new('forumSubjects'),
        ];
    }

}
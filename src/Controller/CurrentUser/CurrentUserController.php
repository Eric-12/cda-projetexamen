<?php

namespace App\Controller\CurrentUser;

use App\Entity\User;
use App\Entity\ForumCommentary;
use App\Entity\ForumSubject;
use App\Services\FileUploader;
use App\Form\UpdatedProfilType;
use App\Form\ChangePasswordFormType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


/**
 * @Route("/profil", name="app_currentUser_")
 */
class CurrentUserController extends AbstractController
{
    /**
     * @Route("/informations", name="profil")
     */
    public function index(): Response
    {
        return $this->render('current_user/profil.html.twig', [
            'controller_name' => 'CurrentUserController::index',
            'title' => 'Espace personnel'
        ]);
    }

    /**
     * @Route("/modifier-mon-compte", name="updatedProfil")
     */
    public function updatedprofil(Request $request, UserInterface $user): Response
    {
        $currentEmailOfUser = $user->getUserIdentifier();
        $currentUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $currentEmailOfUser]);
        $currentPseudo = $currentUser->getPseudo();

        $form = $this->createForm(UpdatedProfilType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Recupère les identifiants de l'utilisateur modifiés dans le formulaire
            $formdata = $form->getData();
            // Récupère le manager pour la persistence des données et la mise à jour de la base de données
            $entityManager = $this->getDoctrine()->getManager();

            // Si le pseudo a ètè modifier par l'utilisateur
            if($currentPseudo != $formdata->getPseudo()) {
                // Alors on modifie les champs pseudo dans les sujets
                $subjects = $formdata->getForumSubjects();
                foreach($subjects as $subject) {
                    $subject->setUserPseudo($formdata->getPseudo());
                    // Peristence du nouveau pseudo dans la table forumSubject utilisé par l'utilisateur
                    $entityManager->persist($subject);
                }
                // Alors on modifie les champs pseudo dans les commentaires
                $commentarys = $formdata->getForumCommentarys();
                foreach($commentarys as $commentary) {
                    $commentary->setUserPseudo($formdata->getPseudo());
                    // Peristence du nouveau pseudo dans la table forumSubject utilisé par l'utilisateur
                    $entityManager->persist($subject);
                }
            }

            // Peristence des identifiants de l'utilisateur
            $entityManager->persist($formdata);
            // Mise à jour de la table User, ForumSubject et forumCommentary
            $entityManager->flush(); 
            // Redirection
            return $this->redirectToRoute('app_currentUser_profil');
        }

        return $this->render('current_user/editer.html.twig', [
            'controller_name' => 'CurrentUserController::updatedprofil',
            'userForm' => $form->createView(),
            'title'=>false,
        ]);
    }

    /**
     * @Route("/changer-mot-de-passe", name="changePassword")
     */
    public function changePassword(Request $request, UserPasswordHasherInterface $userPasswordHasherInterface, UserInterface $user):Response {
         // The token is valid; allow the user to change their password.
         $form = $this->createForm(ChangePasswordFormType::class);
         $form->handleRequest($request);
 
         if ($form->isSubmitted() && $form->isValid()) {
             // Encode(hash) the plain password, and set it.
             $encodedPassword = $userPasswordHasherInterface->hashPassword(
                 $user,
                 $form->get('plainPassword')->getData()
             );
 
             $user->setPassword($encodedPassword);
             $this->getDoctrine()->getManager()->flush();
 
             // The session is cleaned up after the password has been changed.
            //  $this->cleanSessionAfterReset();
 
             return $this->redirectToRoute('app_currentUser');
         }
 
         return $this->render('current_user/changePassword.html.twig', [
             'controller_name' => 'CurrentUserController::changePassword',
             'title'           => 'Réinitialisation du mot de passe',
             'resetForm'       => $form->createView(),
         ]);
    }

    /**
     * @Route("/topics", name="forumTopics")
     */
    public function forumSujects(UserInterface $user, PaginatorInterface $paginator, Request $request): Response {
       $listSubjects = $this->getDoctrine()->getRepository(ForumSubject::class)->findBy(['user' => $user],['createdAt' => 'DESC']);

        // PAGINATION
        $subjectsPagination = $paginator->paginate(
            $listSubjects,
            $request->query->getInt('page', 1), 
            5
            
        );

        $subjectsPagination->setCustomParameters([
            'align' => 'center',
            'size' => 'medium',
            'rounded' => true,
        ]);
        
        return $this->render('current_user/forumTopics.html.twig', [
            'controller_name' => 'ResetPaswwordController',
            'title'           => 'Questions sur le forum',
            'subjectsPagination' => $subjectsPagination,
        ]);
    }

    /**
     * @Route("/commentaires", name="forumComments")
     */
    public function forumComments( UserInterface $user, PaginatorInterface $paginator, Request $request): Response {
        $listComments = $this->getDoctrine()->getRepository(ForumCommentary::class)->findBy(['user' => $user],['createdAt' => 'DESC']);
 
        // PAGINATION
        $commentsPagination = $paginator->paginate(
        $listComments, // Requête contenant les données à paginer
            $request->query->getInt('page', 1),
            5 // Nombre de résultats par page
        );
 
        $commentsPagination->setCustomParameters([
            'align' => 'center',
            'size' => 'medium',
            'rounded' => true,
        ]);
         
        return $this->render('current_user/forumComments.html.twig', [
            'controller_name' => 'ResetPaswwordController',
            'title'           => 'Questions sur le forum',
            'commentsPagination' => $commentsPagination,
        ]);
    }

    /**
     * @Route("/supprimer-mon-compte", name="deleteUser")
     */
    public function deleteUser(UserInterface $user, Request $request) {
        // Restreint l'accés aux utilisateurx authentifier
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $submittedToken = $request->request->get('token');
        if ($this->isCsrfTokenValid('delete-user', $submittedToken)) {
            $entityManager = $this->getDoctrine()->getManager();
            // $entityManager->remove($user);
            // $entityManager->flush();

            $this->addFlash('sup', 'Votre compte à bien été supprimé');
        }

        //  // The session is cleaned up after the password has been changed.
        //  $this->cleanSessionAfterReset();

        //  return $this->redirectToRoute('app_home');
    }

}

<?php

namespace App\EventSubscriber;

// src/EventSubscriber/EasyAdminSubscriber.php
/*
 Composant EventDispatcher 
 Ecouteurs <- répartiteurs d'évenements -> évenements

  Souscripteurs d'événements : contient la liste des événements à écouter

  Dans le cycle de vie d'une application Symfony, de nombreux événements sont disponibles
    kernel.request : envoyé avant que le contrôleur ne soit déterminé, au plus tôt dans le cycle de vie.
    kernel.controller : envoyé après détermination du contrôleur, mais avant son exécution.
    kernel.response : envoyé après que le contrôleur retourne un objet Response.
    kernel.terminate : envoyé après que la réponse est envoyée à l'utilisateur.
    kernel.exception : envoyé si une exception est lancée par l'application.
 */

// hash password
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
// Send email
use App\Security\EmailVerifier;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
// Subscripter Events
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    private EmailVerifier $emailVerifier;
    protected $passwordEncrypt;

    public function __construct(UserPasswordHasherInterface $passwordEncrypt,EmailVerifier $emailVerifier)
    {
        $this->passwordEncrypt = $passwordEncrypt;
        $this->emailVerifier = $emailVerifier;
    }

    // 
    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['encodePasswordNewUser'],
            AfterEntityPersistedEvent::class =>  ['sendEmailAfterPersistNewUser'],
             // BeforeEntityUpdatedEvent::class => ['updateNewPassword'],
        ];
    }

    // hashage du mot de passe lors de la création d'un nouvel utilisateur dans le dashboard easyAdmin
    public function encodePasswordNewUser(BeforeEntityPersistedEvent $event)
    {
        $userEntityInstance = $event->getEntityInstance();
        // Ne retourne rien si ce n'est pas une instance de la classe User 
        if (!($userEntityInstance instanceof User)) {
            return;
        }
        // Hashage du mot de passe basé sur security.yaml config de la class $user
        $hashedPassword = $this->passwordEncrypt->hashPassword(
            $userEntityInstance,
            $userEntityInstance->getPassword()
        );
        // $hashedPassword = $this->passwordEncrypt->hashPassword($userEntityInstance->password);
        // Remplace le mot de passe en clair par celui crypté
        $userEntityInstance->setPassword($hashedPassword);
    }

    public function sendEmailAfterPersistNewUser(AfterEntityPersistedEvent $event) {
        $userEntityInstance = $event->getEntityInstance();
        // Ne retourne rien si ce n'est pas une instance de la classe User 
        if (!($userEntityInstance instanceof User)) {
            return;
        }

        // generate a signed url and email it to the user
        $this->emailVerifier->sendEmailConfirmation('app_verify_email', $userEntityInstance,
        (new TemplatedEmail())
            ->from(new Address('dwwm2021@gmail.com', 'Mail bot'))
            ->to($userEntityInstance->getEmail())
            ->subject('Veuillez confirmer votre email')
            ->htmlTemplate('registration/confirmation_email.html.twig')
        );
    }
}

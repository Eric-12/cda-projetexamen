<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader
{
    private $slugger;
    private $deleteFileUploaded;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function fileUploadAndDeleteOld(UploadedFile $newFileUploaded, $upload_destination, $oldFilename)
    {
        // Rename image file
        $originalFilename = pathinfo($newFileUploaded->getClientOriginalName(), PATHINFO_FILENAME);
        // This is necessary to safely include the file name in the URL.
        $safeFilename = $this->slugger->slug($originalFilename);
        $generatedFileName = $safeFilename.'-'.uniqid().'.'.$newFileUploaded->guessExtension();
        // Move the file to the user folder
        try {
            $newFileUploaded->move($upload_destination, $generatedFileName);
            
        } catch (FileException $e) {

            // ... handle exception if something happens during file upload
        } finally {
            // remove old file
            $this->delete($oldFilename);
        }
        return $generatedFileName;
    }

    public function delete(string $oldFilename){
        try {
            unlink($oldFilename);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
<?php

namespace App\Repository;

use App\Entity\Lamellatype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lamellatype|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lamellatype|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lamellatype[]    findAll()
 * @method Lamellatype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LamellatypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lamellatype::class);
    }

    // /**
    //  * @return Lamellatype[] Returns an array of Lamellatype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lamellatype
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

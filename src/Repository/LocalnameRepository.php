<?php

namespace App\Repository;

use App\Entity\Localname;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Localname|null find($id, $lockMode = null, $lockVersion = null)
 * @method Localname|null findOneBy(array $criteria, array $orderBy = null)
 * @method Localname[]    findAll()
 * @method Localname[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocalnameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Localname::class);
    }

    // /**
    //  * @return Localname[] Returns an array of Localname objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Localname
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

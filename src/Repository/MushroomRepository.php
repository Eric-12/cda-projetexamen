<?php

namespace App\Repository;

use App\Entity\Mushroom;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Mushroom|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mushroom|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mushroom[]    findAll()
 * @method Mushroom[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MushroomRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mushroom::class);
    }

    /**
     * @return Mushroom[] Returns an array of Mushroom objects
     */
    public function findByStartcommonName(string $commonName)
    {
        return $this->createQueryBuilder('m')
            ->where('m.commonname like :commonName')
            ->setParameter('commonName', $commonName.'%')
            ->andWhere('m.visibility = true')
            ->orderBy('m.commonname', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}

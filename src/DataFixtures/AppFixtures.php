<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Edibility;
use App\Entity\Media;
use App\Entity\Mushroom;
use App\Entity\Lamellatype;
use App\Entity\Localname;
use DateTime;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    protected $passwordEncrypt;

    public function __construct(UserPasswordHasherInterface $passwordEncrypt)
    {
        $this->passwordEncrypt = $passwordEncrypt;
    }

    public function load(ObjectManager $manager): void
    {
        /**
         * COMPTE ADMINISTRATEUR
         */
        $userAdmin = new User;
        $plaintextPassword = "123456";
        // Hashage du mot de passe basé sur security.yaml
        $hashedPassword = $this->passwordEncrypt->hashPassword(
            $userAdmin,
            $plaintextPassword
        );
        $userAdmin
        ->setFirstname("Eric")
        ->setLastname("Lanza")
        ->setEmail("lanzae32@gmail.com")
        ->setRoles(["ROLE_ADMIN"])
        ->setPassword($hashedPassword)
        ->setPseudo("Eric")
        ->setAvatarFilename("avatar.png")
        ->setIsVerified(true);

        $manager->persist($userAdmin);
        $manager->flush();


        // **************************************************************
        // COMESTIBILITE
        // **************************************************************
        $edibilityMortel = new Edibility;
        $edibilityMortel
            ->setName("Mortel")
            ->setPath("mortel.png");
        $manager->persist($edibilityMortel);

        $edibilityToxique = new Edibility;
        $edibilityToxique
            ->setName("Toxique")
            ->setPath("toxique.png");
        $manager->persist($edibilityToxique);

        $edibilityRejeter = new Edibility;
        $edibilityRejeter
            ->setName("A rejeter")
            ->setPath("rejeter.png");
        $manager->persist($edibilityRejeter);

        $edibilitymédiocre = new Edibility;
        $edibilitymédiocre
            ->setName("Comestible médiocre")
            ->setPath("mediocre.png");
        $manager->persist($edibilitymédiocre);

        $edibilityBon = new Edibility;
        $edibilityBon
            ->setName("Bon comestible")
            ->setPath("bon.png");
        $manager->persist($edibilityBon);

        $edibilityExcellent = new Edibility;
        $edibilityExcellent
            ->setName("Excellent comestible")
            ->setPath("excellent.png");
        $manager->persist($edibilityExcellent);

        
        // **************************************************************
        // LAMELLES
        // **************************************************************
        $lamelleAdnees = new Lamellatype;
        $lamelleAdnees
            ->setName("Adnées")
            ->setPath("adnees.png");
        $manager->persist($lamelleAdnees);

        $lamelleDecurrentes = new Lamellatype;
        $lamelleDecurrentes
            ->setName("Décurrentes")
            ->setPath("decurrentes.png");
        $manager->persist($lamelleDecurrentes);

        $lamelleEchancrees = new Lamellatype;
        $lamelleEchancrees
            ->setName("Echancrées")
            ->setPath("echancrees.png");
        $manager->persist($lamelleEchancrees);

        $lamelleEmarginees = new Lamellatype;
        $lamelleEmarginees
            ->setName("Emarginées")
            ->setPath("emarginees.png");
        $manager->persist($lamelleEmarginees);

        $lamelleLibres = new Lamellatype;
        $lamelleLibres
            ->setName("Libres")
            ->setPath("libres.png");
        $manager->persist($lamelleLibres);

        $lamelleSecedentes = new Lamellatype;
        $lamelleSecedentes
            ->setName("Sécédentes")
            ->setPath("secedentes.png");
        $manager->persist($lamelleSecedentes);

        $lamelleSinuees = new Lamellatype;
        $lamelleSinuees
            ->setName("Sinuées")
            ->setPath("sinuees.png");
        $manager->persist($lamelleSinuees);

        $lamelleSubdecurrentes = new Lamellatype;
        $lamelleSubdecurrentes
            ->setName("Subdécurrentes")
            ->setPath("subdecurrentes.png");
        $manager->persist($lamelleSubdecurrentes);

        // Mets à jour la base de données
        $manager->flush();


        // **************************************************************
        //   MUSHROOM
        // **************************************************************
        $mushroom = new Mushroom;
        $mushroom
        ->setLamellatype(null)
        ->setEdibility($edibilityExcellent)
        ->setVisibility(true)
        ->setCommonname('Cèpe de Bordeaux')
        ->setLatinname('BOLETUS EDULIS')
        ->setFlesh( 'blanche.')
        ->setHat('de 10 à 20 cm, hémisphérique puis convexe, charnu, de couleur allant du beige au marron à brun noisette ou brun ochracé clair, parfois plus clair selon exposition, à marge épaisse, souvent excédente et généralement soulignée d\'un fin liseré blanchâtre.')
        ->setLamella('tubes fins et serrés prolongés de pores d\'abord de couleur blanche à blanchâtre puis jaune, devenant ensuite verdâtre sur la fin.', 'robuste et ferme, renflé à ventru ou allongé en massue vers la base, de couleur fauve strié d\'un fin réseau blanc en saillie, devenant blanc vers la base.')
        ->setFoot(null)
        ->setHabitat(null)
        ->setComment( 'excellent comestible.');
        $manager->persist($mushroom);

        // MEDIA
        $media = new Media;
        $media
            ->setMushroom($mushroom)
            ->setPath('619d434603bb8195867859.jpg');
        $manager->persist($media);
        $manager->flush();

        $media = new Media;
        $media
            ->setMushroom($mushroom)
            ->setPath('619d434606189839000481.jpg');
        $manager->persist($media);
        $manager->flush();

        $media = new Media;
        $media
            ->setMushroom($mushroom)
            ->setPath('619d434607188955443823.jpg');
        $manager->persist($media);
        $manager->flush();

        $media = new Media;
        $media
            ->setMushroom($mushroom)
            ->setPath('619d434607e45955045587.jpg');
        $manager->persist($media);
        $manager->flush();

        $media = new Media;
        $media
            ->setMushroom($mushroom)
            ->setPath('619d434608b17971041934.jpg');
        $manager->persist($media);
        $manager->flush();

        $media = new Media;
        $media
            ->setMushroom($mushroom)
            ->setPath('619d43460982b650278964.jpg');
        $manager->persist($media);
        $manager->flush();

        $media = new Media;
        $media
            ->setMushroom($mushroom)
            ->setPath('619d43460a50e440742534.jpg');
        $manager->persist($media);
        $manager->flush();
    }
}

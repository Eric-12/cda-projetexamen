<?php

namespace App\Form;

use App\Entity\ForumCommentary;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\NotBlank;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class CommentaryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('commentary', CKEditorType::class, [
                'row_attr' => ['class' => 'mb-3'],
                'attr' => ['class' => 'form-control',
                    'rows' => '5'
                ],
                'label'       => 'Ajouter un commentaire',
                'required'    => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci d\'entrer votre message',
                    ]),
                ],
            ])
            ->add('save', SubmitType::class, [
                'attr'  => ['class' => 'btn btn-success'],
                'label' => 'Répondre',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ForumCommentary::class,
        ]);
    }
}

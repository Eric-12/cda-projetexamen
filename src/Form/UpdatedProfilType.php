<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UpdatedProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // uploader
            // https://github.com/dustin10/VichUploaderBundle/blob/master/docs/form/vich_image_type.md
            ->add('avatarFile', VichImageType::class, [
                'help'         => 'Images accepté jpeg, jpg, png',
                'attr'         =>['class'=>'form-control'],
                'label'        =>'Choisir un fichier pour votre avatar...',
                'label_attr'   =>['class' => 'form-label text-success'],
                'delete_label' => 'Supprimer l\'image ',
                'required'     => false,
                'download_uri'=> false,
                'constraints' => [
                    new File([
                        'maxSize' => '3072k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/jpg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image (jpeg, png)',
                    ])
                ],
            ])
            ->add('lastname', null, [
                'label'      => 'Nom',
                'label_attr' => ['class' => 'form-label fw-bold'],
                'row_attr'   => ['class' => 'mb-3'],
                'attr'       => ['class' => 'form-control'],
            ])
            ->add('firstname', null, [
                'label'      => 'Prénom',
                'label_attr' => ['class' => 'form-label fw-bold'],
                'row_attr'   => ['class' => 'mb-3'],
                'attr'       => ['class' => 'form-control'],
            ])
            ->add('pseudo', null, [
                'label'      => 'Pseudo',
                'label_attr' => ['class' => 'form-label fw-bold'],
                'row_attr'   => ['class' => 'mb-3'],
                'attr'       => ['class' => 'form-control'],
            ])
            ->add('save', SubmitType::class,[
                'attr'  => ['class' => 'btn btn-success mt-3'],
                'label' => 'Sauvegarder les modifications'
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

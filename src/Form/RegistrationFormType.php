<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Mime\Message;
use Symfony\Component\Validator\Constraints\Email;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('pseudo', TextType::class, [
                'required' => true,
                'row_attr' => ['class' => 'form-group'],
                'label' => 'Pseudo',
                'attr'=> ['class' => 'form-control','maxlength' => '12'],
                'help' => '12 caractères maximum',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer votre pseudo',
                    ]),
                    new Length([
                        'max' => 12,
                        'maxMessage' => 'Votre pseudo ne dois pas contenir plus de {{ limit }} caractères.',
                    ]),
                ]
            ])
            ->add('email',EmailType::class, [
                'required' => true,
                'row_attr' => ['class' => 'form-group p-2'],
                'label' => 'Email',
                'attr'=> ['class' => 'form-control'],
                'constraints' => [
                    new Email([
                        'message' => 'L\'adresse {{ value }} n\'est pas valide',
                    ]),
                    new NotBlank([
                        'message' => 'Veuillez entrer une adresse mail',
                    ]),
                ]
            ])       
            ->add('plainPassword', PasswordType::class, [
                'required' => true,
                'row_attr' => ['class' => 'form-group p-2'],
                'label' => 'Mot de passe',
                'attr' => ['class' => 'form-control','autocomplete' => 'new-password'],
                'help_attr' => ['class' => 'text-light'],
                'help' => 'Huit caractères au minimum, au moins une lettre majuscule, une lettre minuscule et un chiffre',
                // 'help' => 'Votre mot de passe doit contenir 8 caractéres au minimum.',
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer un mot de passe',
                    ]),
                    // new Length([
                    //     'min' => 8,
                    //     'minMessage' => 'Votre mot de passe doit avoir au moins {{ limit }} caractères',
                    //     // max length allowed by Symfony for security reasons
                    //     'max' => 200,
                    //     'maxMessage' => 'Votre mot de passe doit avoir au moins {{ limit }} caractères',
                    // ]),
                    new Regex([
                        'pattern' => "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/",
                        'message' => 'Votre mot de passe ne respecte pas le format demandé.'
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\ForumCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ForumCategoryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', EntityType::class, [
                // Nom de la classe de l'entité
                'class' => ForumCategory::class,

                // 'placeholder' => 'Tous les sujets',

                // Propriété qui doit être utilisée pour afficher les entités sous forme de texte dans l’élément HTML
                'choice_label' => 'name',

                /* Renvoie la chaîne « value » pour chaque choix,
                ceci est utilisé dans l’attribut value en HTML et soumis dans les requêtes POST/PUT */
                'choice_value' => 'id',

                // Mise en forme
                'row_attr'   => ['class' => 'col-auto'],
                'attr'       => ['class' => 'form-select'],
                'label'      => 'Catégories : ',
                'label_attr' => ['class' => 'visually-hidden'],

                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ])
            // ->add('all', ButtonType::class, [
            //     'row_attr' => ['class' => 'col-auto'],
            //     'label'    => 'Toutes catégories',
            //     'attr'     => ['class' => 'btn btn-outline-light mb-3'],
            // ])
            ->add('save', SubmitType::class, [
                'row_attr' => ['class' => 'col-auto'],
                'label'    => 'Rechercher dans cette catégorie',
                'attr'     => ['class' => 'btn btn-outline-light mb-3'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ForumCategory::class,
        ]);
    }
}

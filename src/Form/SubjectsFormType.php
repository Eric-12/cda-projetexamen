<?php

namespace App\Form;

use App\Entity\ForumSubject;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SubjectsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'row_attr' => ['class' => 'mb-3'],
                'attr' => ['class' => 'form-control'],
                'label' => 'Quelle est votre question ?',
                'required' => true,
            ])
            ->add('description', CKEditorType::class, [
                'row_attr' => ['class' => 'mb-3'],
                'attr' => ['class' => 'form-control',
                    'rows' => '10'
                ],
                'label' => 'Décrivez ',
                'label_attr' => ['class' => ''],
                // 'required' => true,
                // 'constraints' => [
                //     new NotBlank([
                //         'message' => 'Merci d\'entrer un commentaire',
                //     ]),
                // ],
            ])
            // Par defaut symfony detecte un entityType et affiche dans un select le titre des catégories ( de fct __toString() = title )
            ->add('forumCategory', null, [
                'row_attr' => ['class' => 'mb-3'],
                'attr' => ['class' => 'form-select'],
                'label' => 'Catégories (choix multiples)',
                'label_attr' => ['class' => 'form-label'],
            ])

            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success my-3'],
                'label' => 'Publier',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ForumSubject::class,
        ]);
    }
}

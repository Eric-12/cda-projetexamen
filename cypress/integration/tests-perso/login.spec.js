// login.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

/// <reference types="cypress" />

describe('Login page', () => {
    beforeEach(() => {
        cy.visit('http://projetexamen-cda/authenticator')
    })

    it('Affiche le titre de la page login', () => {
        cy.get('h1').should('have.text', 'Se connecter')
    })

    it('Affiche le formulaire de connexion', () => {
        cy.get('form').children().contains('Email')
        cy.get('form').children().contains('Password')
        cy.get('form').children().get('button').contains('Connexion')
    })

    it('Le formulaire est invalide si le champ email et password n\'est pas renseigné', () => {
        cy.get('article').children().get('form').submit()
    })

    it('Entrez un mauvais email ou mot de passe', () => {
        cy.get('article').children()
            .get('input[type="email"]')
            .type('eric@test.com')
            .should('have.value', 'eric@test.com')
            .get('input[type="password"]')
            .type('derpderp')
            .should('have.value', 'derpderp')

        cy.get('article').children().get('form').submit()

        cy.get('article').children().get('div[class="login rounded text-light"]').children().get('div').contains('Identifiants invalides.')
    })

    it('Vérifie que le nombre de connexion infructueuse ce limite à 3', () => {
        for (let i = 0; i < 3; i++) {
            cy.get('form').children()
                .get('input[type="email"]').type('skipper@gmail.com').should('have.value', 'skipper@gmail.com')
                .get('input[type="password"]').type('Toto123456').should('have.value', 'Toto123456')
            cy.get('form').submit() 
            cy.wait(500)
        }
        cy.get('form').children().contains('Il y a déjà un compte avec cet email') 
    })

})

// Forum.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

describe('Guide des champignons page', () => {
    beforeEach(() => {
        cy.visit('http://projetexamen-cda/guide-des-champignons')
    })

    it('Affiche le titre et sous titre du forum', () => {
        cy.get('article').children().find("section").
    })

    it('nombre de vignettes', () => {
      cy.get('article [class="container listMushrooms"]').children().get('header').children().get('h1').should('have.text', 'Guide des champignons')
  })

    it('cy.request() - verify response using BDD syntax', () => {
        cy.request('http://projetexamen-cda/guide-des-champignons')
        .then((response) => {
          // https://on.cypress.io/assertions
          expect(response).property('status').to.equal(200)

          expect(response.body).find('section').its('length').should('eq', 10)


        //   expect(response).property('body').to.have.property('length')
        //   expect(response).to.include.keys('headers', 'duration')
        })
      }) 

    // it('Effectuer une recherche par nom commun', () => {
    //     cy.get('form[name="search_mushroom_form"]').children()
    //         .get('input[id="search_mushroom_form_name"]')
    //         .type('a')
    //         .should('have.value', 'a')
    //     cy.get('form[name="search_mushroom_form"]').submit()

    // })

})

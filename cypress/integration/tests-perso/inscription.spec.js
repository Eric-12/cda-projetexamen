// inscription.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

describe('Inscription page', () => {
    beforeEach(() => {
        cy.visit('http://projetexamen-cda/register')
    })

    it('Vérifie le titre de la page inscription', () => {
        cy.get('h1').should('have.text', 'Inscription')
    })

    it('Affiche le formulaire de connexion', () => {
        cy.get('form').children().contains('Pseudo')
        cy.get('form').children().contains('Email')
        cy.get('form').children().contains('Mot de passe')
        cy.get('form').children().get('button').contains('Je m\'inscris')
    })

    it('Le formulaire est invalide si les champs ne sont pas renseignés', () => {
        cy.get('form').submit() 
        cy.get('form').children().contains('Veuillez entrer votre pseudo') 
        cy.get('form').children().contains('Veuillez entrer une adresse mail')
        cy.get('form').children().contains('Veuillez entrer un mot de passe')  
    })

    it('Le formulaire est invalide si le champ pseudo dépasse 12 caratères', () => {
        cy.get('form').children()
            .get('input[id="registration_form_pseudo"]').invoke('val', 'pseudoBeaucoupTropLong')
            .get('input[type="email"]').type('toto@gmail.com').should('have.value', 'toto@gmail.com')
            .get('input[type="password"]').type('Toto123456').should('have.value', 'Toto123456')
        cy.get('form').submit() 
        cy.get('form').children().contains('Votre pseudo ne dois pas contenir plus de 12 caractères.') 
    })

    it('Le formulaire est invalide si le champ email ne respecte pas le format', () => {
        cy.get('form').children()
            .get('input[id="registration_form_pseudo"]').type('newpseudo')
            .get('input[type="email"]').type('toto-gmail.com').should('have.value', 'toto-gmail.com')
            .get('input[type="password"]').type('Toto123456').should('have.value', 'Toto123456')
        cy.get('form').submit() 
        cy.get('form').children().contains('L\'adresse \"toto-gmail.com\" n\'est pas valide') 
    })

    it('Le formulaire est invalide si le champ password ne respecte pas le format', () => {
        cy.get('form').children()
            .get('input[id="registration_form_pseudo"]').type('newpseudo')
            .get('input[type="email"]').type('toto2@gmail.com').should('have.value', 'toto2@gmail.com')
            .get('input[type="password"]').type('toto').should('have.value', 'toto')
        cy.get('form').submit() 
        cy.get('form').children().contains('Votre mot de passe ne respecte pas le format demandé.') 
    })

    it('Le formulaire est invalide si le compte existe', () => {
        cy.get('form').children()
            .get('input[id="registration_form_pseudo"]').type('newpseudo')
            .get('input[type="email"]').type('skipper@gmail.com').should('have.value', 'skipper@gmail.com')
            .get('input[type="password"]').type('Toto123456').should('have.value', 'Toto123456')
        cy.get('form').submit() 
        cy.get('form').children().contains('Il y a déjà un compte avec cet email') 
    }) 

})
// email.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

describe('Inscription page', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8080/contact')
    })

    it('Vérifie le titre de la page', () => {
        cy.get('h1').should('have.text', 'Formulaire de contact')
    })

    it('Affiche le formulaire de connexion', () => {
        cy.get('form').children().get('input[id="contact_form_lastname"]')
        cy.get('form').children().get('input[id="contact_form_firstname"]')
        cy.get('form').children().get('input[id="contact_form_email"]')
        cy.get('form').children().get('input[id="contact_form_phone"]')
        cy.get('form').children().get('textarea[id="contact_form_message"]')
        cy.get('form').children().get('button').contains('Envoyer')
    })

    it('Le formulaire n\'est pas envoyé si les champs nom prénom email phone message ne sont pas renseigné', () => {
        cy.get('article').children().get('form').submit()
    })


    // it('Le formulaire est invalide si le champ email n\'est pas renseigné', () => {
    //     cy.get('form').children().contains('Pseudo').type('test')
    //     cy.get('form').children().contains('Mot de passe').type('1237889')
    //     cy.get('article').children().get('form').submit()
    // })

    // it('Le formulaire est invalide si le champ password n\'est pas renseigné', () => {
    //     cy.get('form').children().contains('Pseudo').type('test')
    //     cy.get('form').children().contains('Email').type('test@test.fr')
    //     cy.get('article').children().get('form').submit()
    // })

    // it('Le formulaire est invalide si tous les champs ne sont pas renseigné', () => {
    //     cy.get('article').children().get('form').submit()
    // })

    // it('Le formulaire est invalide si le champ email ne respecte pas le format', () => {
    //     cy.get('form').children().contains('Pseudo').type('test')
    //     cy.get('form').children().contains('Email').type('test.fr')
    //     cy.get('form').children().contains('Mot de passe').type('1237889')
    //     cy.get('article').children().get('form').submit()
    // })

})
<?php

namespace App\Tests\Entity;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class UserEntityTest extends KernelTestCase
{
    private const NOT_BLANK_PSEUDO = "Veuillez entrer un pseudo";
    private const NOT_MAX_LENGHT_PSEUDO =  'Votre pseudo ne dois pas contenir plus de 12 caractères.';


    private const NOT_BLANK_CONTRAINT_EMAIL_MESSAGE = 'Veuillez saisir une adresse mail';
    private const EMAIL_CONTRAINT_MESSAGE =  'L\'email \"test-email@gmail\" n\'est pas valide';
    private const VALID_EMAIL_VALUE = 'test-email@gmail.com';


    private const NOT_BLANK_PASSWORD = "Veuillez entrer un pseudo";
    private const PASSWORD_CONTRAINT_MESSAGE =  'L\'email \"test-email@gmail\" n\'est pas valide';
    private const VALID_PASSWORD_VALUE = 'Toto123456';
    private const INVALID_PASSWORD_VALUE = 'toto';
   
    // private ValidatorInterface $validator;
    // protected function setUp():void 
    // {
    //     $kernel = self::bootKernel();
    //     $this->validator =  $kernel->getContainer()->get('validator');
    // }


    public function testUserEntityIsValid(): void
    {
        $user = (new User());
        $user
            ->setEmail(self::VALID_EMAIL_VALUE)
            ->setPassword(self::VALID_PASSWORD_VALUE);
        
        $this->getValidationErrors($user,0);
    }

    public function testUserEntityPasswordIsNotValid(): void
    {
        $user = (new User());
        $user
            ->setEmail(self::VALID_EMAIL_VALUE)
            ->setPassword(self::INVALID_PASSWORD_VALUE);
        
       $kernel = self::bootKernel();
       $error =  $kernel->getContainer()->get('validator')->validate($user);
       $this->assertCount(1,$error);
    }


    public function testUserEntityIsInvalidBecauseNoEmailEntered():void
    {
        $user = (new User());
        $user
            ->setPassword(self::VALID_PASSWORD_VALUE);

        // on attends une erreur car on a pas renseigné la propriété email
        $errors = $this->getValidationErrors($user,1);
        $this->assertEquals(self::NOT_BLANK_CONTRAINT_EMAIL_MESSAGE, $errors[0]->getMessage());
    }


    private function getValidationErrors(User $user, int $numberOfExceptedErrors ):ConstraintViolationList {
        $kernel = self::bootKernel();
        
        $errors =  $kernel->getContainer()->get('validator')->validate($user);
        $this->assertCount($numberOfExceptedErrors, $errors);
        return $errors;
    }
}

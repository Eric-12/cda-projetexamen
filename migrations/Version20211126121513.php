<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211126121513 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mushroom DROP FOREIGN KEY FK_E7B358EBA76ED395');
        $this->addSql('DROP INDEX IDX_E7B358EBA76ED395 ON mushroom');
        $this->addSql('ALTER TABLE mushroom DROP user_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mushroom ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE mushroom ADD CONSTRAINT FK_E7B358EBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_E7B358EBA76ED395 ON mushroom (user_id)');
    }
}

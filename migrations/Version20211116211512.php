<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211116211512 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE edibility (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) DEFAULT NULL, path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lamellatype (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) DEFAULT NULL, path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE localname (id INT AUTO_INCREMENT NOT NULL, mushroom_id INT NOT NULL, created_at DATETIME DEFAULT NULL, name VARCHAR(100) DEFAULT NULL, INDEX IDX_FC665D8E3FCCFFC (mushroom_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media (id INT AUTO_INCREMENT NOT NULL, mushroom_id INT NOT NULL, created_at DATETIME DEFAULT NULL, name VARCHAR(100) DEFAULT NULL, path VARCHAR(255) DEFAULT NULL, INDEX IDX_6A2CA10CE3FCCFFC (mushroom_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mushroom (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, lamellatype_id INT DEFAULT NULL, edibility_id INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, visibility TINYINT(1) DEFAULT NULL, commonname VARCHAR(100) NOT NULL, latinname VARCHAR(100) DEFAULT NULL, flesh LONGTEXT DEFAULT NULL, hat LONGTEXT DEFAULT NULL, lamella LONGTEXT DEFAULT NULL, foot LONGTEXT DEFAULT NULL, habitat LONGTEXT DEFAULT NULL, comment LONGTEXT DEFAULT NULL, slug VARCHAR(150) NOT NULL, UNIQUE INDEX UNIQ_E7B358EB989D9B62 (slug), INDEX IDX_E7B358EBA76ED395 (user_id), INDEX IDX_E7B358EB6655602 (lamellatype_id), INDEX IDX_E7B358EB5B62C8FD (edibility_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(100) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, is_verified TINYINT(1) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, lastname VARCHAR(100) DEFAULT NULL, firstname VARCHAR(100) DEFAULT NULL, pseudo VARCHAR(100) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE localname ADD CONSTRAINT FK_FC665D8E3FCCFFC FOREIGN KEY (mushroom_id) REFERENCES mushroom (id)');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10CE3FCCFFC FOREIGN KEY (mushroom_id) REFERENCES mushroom (id)');
        $this->addSql('ALTER TABLE mushroom ADD CONSTRAINT FK_E7B358EBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE mushroom ADD CONSTRAINT FK_E7B358EB6655602 FOREIGN KEY (lamellatype_id) REFERENCES lamellatype (id)');
        $this->addSql('ALTER TABLE mushroom ADD CONSTRAINT FK_E7B358EB5B62C8FD FOREIGN KEY (edibility_id) REFERENCES edibility (id)');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mushroom DROP FOREIGN KEY FK_E7B358EB5B62C8FD');
        $this->addSql('ALTER TABLE mushroom DROP FOREIGN KEY FK_E7B358EB6655602');
        $this->addSql('ALTER TABLE localname DROP FOREIGN KEY FK_FC665D8E3FCCFFC');
        $this->addSql('ALTER TABLE media DROP FOREIGN KEY FK_6A2CA10CE3FCCFFC');
        $this->addSql('ALTER TABLE mushroom DROP FOREIGN KEY FK_E7B358EBA76ED395');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('DROP TABLE edibility');
        $this->addSql('DROP TABLE lamellatype');
        $this->addSql('DROP TABLE localname');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE mushroom');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE user');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211214080723 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE forum_commentary DROP FOREIGN KEY FK_9E9AAF8F800861C2');
        $this->addSql('ALTER TABLE forum_commentary ADD CONSTRAINT FK_9E9AAF8F800861C2 FOREIGN KEY (forum_subject_id) REFERENCES forum_subject (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE forum_commentary DROP FOREIGN KEY FK_9E9AAF8F800861C2');
        $this->addSql('ALTER TABLE forum_commentary ADD CONSTRAINT FK_9E9AAF8F800861C2 FOREIGN KEY (forum_subject_id) REFERENCES forum_subject (id)');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211204083745 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE contact ADD slug VARCHAR(150) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4C62E638989D9B62 ON contact (slug)');
        $this->addSql('ALTER TABLE edibility ADD slug VARCHAR(150) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3F100121989D9B62 ON edibility (slug)');
        $this->addSql('ALTER TABLE forum_category ADD slug VARCHAR(150) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_21BF9426989D9B62 ON forum_category (slug)');
        $this->addSql('ALTER TABLE forum_commentary ADD slug VARCHAR(150) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9E9AAF8F989D9B62 ON forum_commentary (slug)');
        $this->addSql('ALTER TABLE forum_subject ADD slug VARCHAR(200) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A02730B989D9B62 ON forum_subject (slug)');
        $this->addSql('ALTER TABLE lamellatype ADD slug VARCHAR(150) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A32D09989D9B62 ON lamellatype (slug)');
        $this->addSql('ALTER TABLE localname ADD slug VARCHAR(150) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FC665D8989D9B62 ON localname (slug)');
        $this->addSql('ALTER TABLE user ADD slug VARCHAR(150) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649989D9B62 ON user (slug)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_4C62E638989D9B62 ON contact');
        $this->addSql('ALTER TABLE contact DROP slug');
        $this->addSql('DROP INDEX UNIQ_3F100121989D9B62 ON edibility');
        $this->addSql('ALTER TABLE edibility DROP slug');
        $this->addSql('DROP INDEX UNIQ_21BF9426989D9B62 ON forum_category');
        $this->addSql('ALTER TABLE forum_category DROP slug');
        $this->addSql('DROP INDEX UNIQ_9E9AAF8F989D9B62 ON forum_commentary');
        $this->addSql('ALTER TABLE forum_commentary DROP slug');
        $this->addSql('DROP INDEX UNIQ_A02730B989D9B62 ON forum_subject');
        $this->addSql('ALTER TABLE forum_subject DROP slug');
        $this->addSql('DROP INDEX UNIQ_A32D09989D9B62 ON lamellatype');
        $this->addSql('ALTER TABLE lamellatype DROP slug');
        $this->addSql('DROP INDEX UNIQ_FC665D8989D9B62 ON localname');
        $this->addSql('ALTER TABLE localname DROP slug');
        $this->addSql('DROP INDEX UNIQ_8D93D649989D9B62 ON user');
        $this->addSql('ALTER TABLE user DROP slug');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211201231801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE forum_category (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE forum_subject_forum_category (forum_subject_id INT NOT NULL, forum_category_id INT NOT NULL, INDEX IDX_3F57A4E3800861C2 (forum_subject_id), INDEX IDX_3F57A4E314721E40 (forum_category_id), PRIMARY KEY(forum_subject_id, forum_category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE forum_subject_forum_category ADD CONSTRAINT FK_3F57A4E3800861C2 FOREIGN KEY (forum_subject_id) REFERENCES forum_subject (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE forum_subject_forum_category ADD CONSTRAINT FK_3F57A4E314721E40 FOREIGN KEY (forum_category_id) REFERENCES forum_category (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE forum_subject_forum_commentary');
        $this->addSql('ALTER TABLE forum_commentary ADD forum_subject_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE forum_commentary ADD CONSTRAINT FK_9E9AAF8F800861C2 FOREIGN KEY (forum_subject_id) REFERENCES forum_subject (id)');
        $this->addSql('CREATE INDEX IDX_9E9AAF8F800861C2 ON forum_commentary (forum_subject_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE forum_subject_forum_category DROP FOREIGN KEY FK_3F57A4E314721E40');
        $this->addSql('CREATE TABLE forum_subject_forum_commentary (forum_subject_id INT NOT NULL, forum_commentary_id INT NOT NULL, INDEX IDX_ECEA2C0800861C2 (forum_subject_id), INDEX IDX_ECEA2C0B56D436F (forum_commentary_id), PRIMARY KEY(forum_subject_id, forum_commentary_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE forum_subject_forum_commentary ADD CONSTRAINT FK_ECEA2C0800861C2 FOREIGN KEY (forum_subject_id) REFERENCES forum_subject (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE forum_subject_forum_commentary ADD CONSTRAINT FK_ECEA2C0B56D436F FOREIGN KEY (forum_commentary_id) REFERENCES forum_commentary (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE forum_category');
        $this->addSql('DROP TABLE forum_subject_forum_category');
        $this->addSql('ALTER TABLE forum_commentary DROP FOREIGN KEY FK_9E9AAF8F800861C2');
        $this->addSql('DROP INDEX IDX_9E9AAF8F800861C2 ON forum_commentary');
        $this->addSql('ALTER TABLE forum_commentary DROP forum_subject_id');
    }
}

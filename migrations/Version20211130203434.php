<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211130203434 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE forum_commentary (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', commentary LONGTEXT NOT NULL, INDEX IDX_9E9AAF8FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE forum_subject (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_A02730BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE forum_subject_forum_commentary (forum_subject_id INT NOT NULL, forum_commentary_id INT NOT NULL, INDEX IDX_ECEA2C0800861C2 (forum_subject_id), INDEX IDX_ECEA2C0B56D436F (forum_commentary_id), PRIMARY KEY(forum_subject_id, forum_commentary_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE forum_commentary ADD CONSTRAINT FK_9E9AAF8FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE forum_subject ADD CONSTRAINT FK_A02730BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE forum_subject_forum_commentary ADD CONSTRAINT FK_ECEA2C0800861C2 FOREIGN KEY (forum_subject_id) REFERENCES forum_subject (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE forum_subject_forum_commentary ADD CONSTRAINT FK_ECEA2C0B56D436F FOREIGN KEY (forum_commentary_id) REFERENCES forum_commentary (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE forum_subject_forum_commentary DROP FOREIGN KEY FK_ECEA2C0B56D436F');
        $this->addSql('ALTER TABLE forum_subject_forum_commentary DROP FOREIGN KEY FK_ECEA2C0800861C2');
        $this->addSql('DROP TABLE forum_commentary');
        $this->addSql('DROP TABLE forum_subject');
        $this->addSql('DROP TABLE forum_subject_forum_commentary');
    }
}

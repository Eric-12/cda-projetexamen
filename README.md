# CDA-ProjetExamen


## Présentation

J’ai choisi de réaliser un projet personnel, ayant l’avantage d’aborder toutes les
étapes de développement d’un site web et de remplir les critères d’évaluation de mes
compétences professionnelles.

Ce projet est une petite encyclopédie ou guide permettant d'identifier les différentes espèces de champignons, de partager à travers un forum sur des sujets autour de ce thème et permettre aux membres de la communauté de publier des articles.

Titre possible pour le site : le royaume des champignons



## Vision du produit

Le pattern “QQOQCP” – Qui, Quoi, Où, Quand, Comment, Pourquoi 

![QQOQCP-cercle-dor-1024x755](https://i2.wp.com/blog.zenika.com/wp-content/uploads/2019/06/QQOQCP-cercle-dor-1024x755.jpg?resize=500%2C368&ssl=1)

Les utilisations principales, l'environnement, les limites.



### Pourquoi - quel est le but recherché en proposant cette solution

1. Permettre d'identifier les différentes espèces de champignons à travers un ensemble de fiches signalétiques.

2. Favoriser les échanger entre membres du site sur des sujets accessibles par date ou par thèmes. (forum)

3. Patager ses connaissances avec la publication d'articles personnels dans espace blog.


### Comment - Comment sera réalisée la solution et quels sont les moyens mise en œuvre.

1. Réaliser le product **Vision Board**.
2. **Personnas** : parcours utilisateur en déterminant différent profils (2 profil à minima )
3. **Maquettage**
   - **Wireframe** des différents écrans
   - **Story-board** : enchaînement des écran sur la base du Wireframe
   - **Site Maps** : plan du site sous forme graphique
   - **MoodBoard** : representation proche du produit final.
4. **Backlog** : Rassemble les <span style="color:red">U<span>ser <span style="color:red">S<span>tories dans un Product Backlog.
5. **User Stories** : fonctionnalités raconté du point de vu utilisateur organisé.
6. **Tableau de bord** : Organiser et prioriser à travers des tableaux l'avancement du projet.
7. **Choix des technologies pour le développement du produit**
   - Symfony avec twig
   - Api java (technos appliqué pendant mon alternance) et front avec angular ou vueJs.



### QUOI - Les  fonctionnalités (features) accessibles à l'ensemble des utilisateurs.

1. Ce site proposera un ensemble de fiche signalétiques permettant d'identifier les différentes espèces de champignons :
   - Organisées sous forme de vignettes trié par nom commun avec un système de pagination.
   - Propose un moteur de recherche par nom commun. 
2. Un forum ou espace de discussion ouvert au internaute ayant crée un compte gratuit .
   - Affichera les derniers sujets triés par ordre chronologique.
   - Propose un moteur de recherche par sujet.
3. Un espace blog pour la publication périodique et régulière d'articles personnels. Ces billets classé par thèmes seront accessible au visiteur. (recette culinaire, méthode de culture, les espèces et les régions, conseilles divers).


### QUI - qui sont les utilisateurs/clients futurs et quelle est la cible à atteindre

j’ai défini trois types d’utilisateurs:

- administrateur 
- Membres.
- Visiteurs.



### OU -  y'a t'il des contraintes géographiques

Aucune contrainte géographiques



### Quand - As t'on une date cible à atteindre

Le site devra être accessible au plus tard pour le mois de février.



## Fonctionnalités en fonction des droits



### User stories

En tant que administateur du site je souhaite 



## Fonctionnalités à venir.

Proposer à ceux qui le désirent de participer à l’élaboration d’une base de données collaboratives, regroupant photos et descriptions d’espèces.




## THE PRODUCT VISION BOARD

 [vision board.xlsx](https://docs.google.com/spreadsheets/d/1xYMw7ULVWcYiW-OEbADTXR98qHqb3wUuL4dtzjQ0cg8/edit?usp=sharing) 

## Personnas

![Diapositive11-500x375](./img-projet_examen/Diapositive11-500x375.jpg)

